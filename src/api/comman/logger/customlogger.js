const express = require('express');
const router = express.Router();
var mongoose = require('mongoose');
const ErrorLoggerModel = require('../../../app/models/errorlogger');
router.post('/', function (req, res) {
    
    var ErrorLoggerData = new ErrorLoggerModel({
        time: req.body['time'],
        url: req.body['url'],
        status: req.body['status'],
        message: req.body['message'],
      });

      ErrorLoggerData.save(function(err,result){
          if(err){
              res.json({
                  status:400
              })
          }else{
            res.json({
                data:result,
                status:200
            })
          }
      })
});

module.exports = router;