const express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
const async = require('async');
var priorityRulesModel = require('../../app/models/priority-rules');
const cipher = require('../comman/auth/ciperHelper');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
var encrypt = require('../../libs/encrypt');
var validator = require('../../middleware/validator');
const emailvalidator = require('email-validator');
const authController = require('../../app/controller/authController');
const dbController = require('../../app/controller/dbController')

router.get('/demoRoute', (req, res) => {
    res.send('Hello World!')
});

/*
    Save Client Informations (Date: 25-02-2022)
*/
router.post('/saveWebsites', validator.emailExist,authController.saveWebsites);

/*
    Use this api for login user  (Date: 26-02-2022)
*/
router.post('/login', authController.login);


// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Fulfillment Priority Rules (Date: 25-02-2022)
*/
router.post('/savePriorityRules', authController.savePriorityRules)

/*
    Get Fulfillment Priority Rules Using tenant_id and rule_code (Date: 25-02-2022)
*/
router.post('/getPriorityRules', authController.getPriorityRules)

/*
    Get route by dynamic mysql database connection (Date: 02-03-2022)
*/
router.post('/getCourierData', authController.getCourierData)

/*
    Get recommendation priority rule api (Date: 05-03-2022)
*/
router.post('/getRecommendationPriority', authController.getRecommendationPriority)

/*
    Search client name  (Date: 25-03-2022)
*/
router.post('/searchClientName', authController.searchClientName)

/*
    validation Fulfillment Priority Rules api With Existing Rule Validation (Date: 25-03-2022)
*/
router.post('/validationPriorityRules', authController.validationPriorityRules)


/*
    Delete recommendations rules api using tenant_id and rule_code (Date: 29-03-2022)
*/
router.delete('/removePriorityRules', authController.removePriorityRules)


module.exports = router;