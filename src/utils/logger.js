const winston = require('winston')
var sendgrid = require('@sendgrid/mail');
const Mail = require('winston-mail').Mail;
var emailService = require('../utils/emailService')


const options = {
  file: {
    level: 'info',
    filename: './logs/app.log',
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  }
};
const logger = winston.createLogger({
  levels: winston.config.npm.levels,
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console),
  ],
  exceptionHandlers: [],
  exitOnError: false
})
process.on('uncaughtException', function (err, res) {
  emailService.sgMail(err)
  logger.error('uncaughtException', { message: err.message, stack: err.stack }); // logging with MetaData
  // process.exit(1); // exit with failure
});

process.on('unhandledRejection', (err, promise) => {
  logger.error('uncaughtException', { message: err.message, stack: err.stack }); // logging })
});
module.exports = logger