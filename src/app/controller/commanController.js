const priorityRulesModel = require('../models/priority-rules');
var trim = require('trim');


exports.recommendedPriorityRule = async (req, zone) => {
  var priorityRules = [];
  console.log("zone@@---", zone)
  if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')
    && req.body.pay_type && (req.body.pay_type !== undefined || req.body.pay_type !== 'undefined' || req.body.pay_type !== '')
    && req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')) {
    if (zone && req.body.weight && (zone !== '' || zone !== undefined) && (req.body.weight !== undefined || req.body.weight !== 'undefined' || req.body.weight !== '')) {
      console.log("req.body---",req.body)
      var tenant_id = trim(req.body.tenant_id)
      var pay_type = trim(req.body.pay_type)
      var weight = trim(req.body.weight)
      var zone = trim(zone)
      var weight1 = parseFloat(weight)
      console.log("weight1---",weight1)
      if (pay_type) {
        priorityRules = await priorityRulesModel.find({
          tenant_id: tenant_id,
          pay_type:  pay_type.toUpperCase(),
          zone: zone,
          rule_code: 'WZ',          
          from: { "$lt": weight1 },
          to: { "$gte": weight1 },
        });
      }

      if (priorityRules.length == 0) {
        priorityRules = await priorityRulesModel.find({
          tenant_id: tenant_id,
          pay_type: 'BOTH',
          rule_code: 'WZ',
          zone: zone,
          from: { "$lt": weight1 },
          to: { "$gte": weight1 },
       });
      }
    }
    if (priorityRules.length == 0) {
      if (zone && (zone !== '' || zone !== undefined) && (req.body.weight === undefined || req.body.weight === 'undefined' || req.body.weight === '' || req.body.weight)) {
        // console.log("zone rule")
        var tenant_id = trim(req.body.tenant_id)
        var pay_type = trim(req.body.pay_type)
        var zone = trim(zone)
        if (pay_type) {
          priorityRules = await priorityRulesModel.find({
            tenant_id: tenant_id,
            rule_code: 'ZM',
            pay_type: pay_type.toUpperCase(),
            zone: zone,
          });
        }
        if (priorityRules.length == 0) {
          priorityRules = await priorityRulesModel.find({
            tenant_id: tenant_id,
            rule_code: 'ZM',
            pay_type: 'BOTH',
            zone: zone,
          });
        }

      }
    }
    if (priorityRules.length == 0) {
      if (req.body.weight && (zone === '' || req.body.weight !== undefined || zone)) {
        // console.log("weight rule")
        var tenant_id = trim(req.body.tenant_id)
        var pay_type = trim(req.body.pay_type)
        var weight = trim(req.body.weight)
        var weight1 = parseFloat(weight)
        console.log("weight1---",weight1)
        console.log("weight0---",weight)
        if (pay_type) {
          priorityRules = await priorityRulesModel.find({
            tenant_id: tenant_id,
            rule_code: 'WM',
            pay_type: pay_type.toUpperCase(),
            from: { "$lt": weight1 },
            to: { "$gte":weight1 },
            });
        }
        if (priorityRules.length == 0) {
          priorityRules = await priorityRulesModel.find({
            tenant_id: tenant_id,
            rule_code: 'WM',
            pay_type: 'BOTH',
            from: { "$lt": weight1 },
            to: { "$gte": weight1 }
            });
        }

      }

    }
    
    if(priorityRules.length == 0){
      if(pay_type){
        priorityRules = await priorityRulesModel.find({
          tenant_id: tenant_id,
          pay_type: pay_type.toUpperCase(),
          rule_code: 'default_rule'
        });
      }
      if(priorityRules.length == 0){
        priorityRules = await priorityRulesModel.find({
          tenant_id: tenant_id,
          pay_type: 'BOTH',
          rule_code: 'default_rule'
        });
      }
     
    }

  } else {
    // return res.status(400).json({
    priorityRules = 'All fields are required....'
    // })
  }
  return priorityRules

}

exports.recommendedPriorityRule2 = async (priorityRules, results3) => {
  console.log("req----", results3)

  console.log("priorityRules----", priorityRules)
  var newdata = []
  console.log("results3---", results3)
  priorityRules.forEach(function (data) {
    console.log("data.list---", data.list)
    for (var i = 0; i <= data.list.length - 1; i++) {
      for (var j = 0; j < results3.length; j++) {
        // console.log("-----",data.list[i].courier_code,results3[j].courier_id)
        if (data.list[i] && (data.list[i].courier_code === results3[j].courier_id)) {

          newdata.push({
            courier_code: data.list[i].courier_code
          })
        }
      }
    }
  })

  console.log("newdata---", newdata)
  var newdata2 = []
  priorityRules.forEach(function (data) {
    newdata2.push({
      _id: data._id,
      tenant_id: data.tenant_id,
      rule_code: data.rule_code,
      list: newdata
    })
  })

  console.log("newdata2---", newdata2)
  return newdata2

}



// exports.recommendedPriorityRule2 = async(req,zone,results3) =>{
//   console.log("req----",results3)
//   console.log("zone@@@---",zone)
//   console.log("req.body---",req.body)
// var priorityRules = []
//   if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')
//     && req.body.pay_type && (req.body.pay_type !== undefined || req.body.pay_type !== 'undefined' || req.body.pay_type !== '')
//     && req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')) {
//     if (zone && req.body.weight && (zone !== '' || zone !== undefined) && (req.body.weight !== undefined || req.body.weight !== 'undefined' || req.body.weight !== '')) {
//       // console.log("zone + weight")
//       var tenant_id = trim(req.body.tenant_id)
//       var pay_type = trim(req.body.pay_type)
//       var weight = trim(req.body.weight)
//       var zone = trim(zone)

//       if (pay_type) {
//         priorityRules = await priorityRulesModel.find({
//           tenant_id: tenant_id,
//           pay_type:  pay_type.toUpperCase(),
//           zone: zone,
//           rule_code: 'WZ',
//           to: { "$gte": weight },
//           from: { "$lte": weight },
//         });
//       }

//       if (priorityRules.length == 0) {
//         priorityRules = await priorityRulesModel.find({
//           tenant_id: tenant_id,
//           pay_type: 'BOTH',
//           rule_code: 'WZ',
//           zone: zone,
//           to: { "$gte": weight },
//           from: { "$lte": weight },
//         });
//       }
//     }
//     if (priorityRules.length == 0) {
//       if (zone && (zone !== '' || zone !== undefined) && (req.body.weight === undefined || req.body.weight === 'undefined' || req.body.weight === '' || req.body.weight)) {
//         // console.log("zone rule")
//         var tenant_id = trim(req.body.tenant_id)
//         var pay_type = trim(req.body.pay_type)
//         var zone = trim(zone)
//         if (pay_type) {
//           priorityRules = await priorityRulesModel.find({
//             tenant_id: tenant_id,
//             rule_code: 'ZM',
//             pay_type: pay_type.toUpperCase(),
//             zone: zone,
//           });
//         }
//         if (priorityRules.length == 0) {
//           priorityRules = await priorityRulesModel.find({
//             tenant_id: tenant_id,
//             rule_code: 'ZM',
//             pay_type: 'BOTH',
//             zone: zone,
//           });
//         }

//       }
//     }
//     if (priorityRules.length == 0) {
//       if (req.body.weight && (zone === '' || req.body.weight !== undefined || zone)) {
//         // console.log("weight rule")
//         var tenant_id = trim(req.body.tenant_id)
//         var pay_type = trim(req.body.pay_type)
//         var weight = trim(req.body.weight)
//         if (pay_type) {
//           priorityRules = await priorityRulesModel.find({
//             tenant_id: tenant_id,
//             rule_code: 'WM',
//             pay_type: pay_type.toUpperCase(),
//             to: { "$gte": weight },
//             from: { "$lte": weight },
//           });
//         }
//         if (priorityRules.length == 0) {
//           priorityRules = await priorityRulesModel.find({
//             tenant_id: tenant_id,
//             rule_code: 'WM',
//             pay_type: 'BOTH',
//             to: { "$gte": weight },
//             from: { "$lte": weight },
//           });
//         }

//       }

//     }
    
//     if(priorityRules.length == 0){
//       if(pay_type){
//         priorityRules = await priorityRulesModel.find({
//           tenant_id: tenant_id,
//           pay_type: pay_type.toUpperCase(),
//           rule_code: 'default_rule'
//         });
//       }
//       if(priorityRules.length == 0){
//         priorityRules = await priorityRulesModel.find({
//           tenant_id: tenant_id,
//           pay_type: 'BOTH',
//           rule_code: 'default_rule'
//         });
//       }
     
//     }
//     console.log("priorityRules----",priorityRules)
//     var newdata = []
//     console.log("results3---",results3)
//     priorityRules.forEach(function(data){
//       console.log("data.list---",data.list)
//       for( var i=0; i<=data.list.length - 1; i++){
//         for( var j=0; j<results3.length; j++){
//           // console.log("-----",data.list[i].courier_code,results3[j].courier_id)
//             if(data.list[i] && (data.list[i].courier_code === results3[j].courier_id)){
                
//                 newdata.push({
//                  courier_code: data.list[i].courier_code
//                 })
//             }
//         }
//     }
//     })
   
    
//     console.log("newdata---",newdata)
//   var newdata2 = []
//     priorityRules.forEach(function(data){
//       newdata2.push({
//         _id: data._id,
//         tenant_id: data.tenant_id,
//         rule_code: data.rule_code,
//         list: newdata
//       })
//     })

//   } else {
//     // return res.status(400).json({
//       newdata2 = 'All fields are required....'
//     // })
//   }

//   console.log("newdata2---",newdata2)
//   return newdata2

// }