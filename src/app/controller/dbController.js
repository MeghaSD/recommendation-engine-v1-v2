const mysql = require('mysql2');
const dotenv = require('dotenv');
dotenv.config({path:__dirname+'/.env'});
const CLIENT_HOST = process.env.CLIENT_HOST
const CLIENT_PORT = process.env.CLIENT_PORT
const CLIENT_USER = process.env.CLIENT_USER
const CLIENT_PASSWORD = process.env.CLIENT_PASSWORD
const SYSTEM_HOST = process.env.SYSTEM_HOST
const SYSTEM_PORT = process.env.SYSTEM_PORT
const SYSTEM_USER = process.env.SYSTEM_USER
const SYSTEM_PASSWORD = process.env.SYSTEM_PASSWORD
const SYSTEM_DB_NAME = process.env.SYSTEM_DB_NAME




exports.getMysqlDBMaster = async(req, res, next) => {
    // console.log("req.params@---",req.params)
    const connection2 = mysql.createConnection({
        host: SYSTEM_HOST, // host for connection
        port: SYSTEM_PORT, // default port for mysql is 3306
        database: SYSTEM_DB_NAME,//'system', // database from which we want to connect out node application
        user: SYSTEM_USER, // username of the mysql connection
        password: SYSTEM_PASSWORD// password of the mysql connection
        });
      
       connection2.connect(function (err,result,next) {
          if(err){
              console.log("error occured while connecting");
          }
          else{
              console.log("connection created with Mysql successfully");
          }
       });
      return connection2
}


exports.getMysqlDBClient = async(database, res, next) => {
    const connection2 = mysql.createConnection({
        host: CLIENT_HOST, // host for connection
        port: CLIENT_PORT, // default port for mysql is 3306
        database: database,//'system', // database from which we want to connect out node application
        user: CLIENT_USER, // username of the mysql connection
        password: CLIENT_PASSWORD // password of the mysql connection
        });
      
       connection2.connect(function (err,result,next) {
          if(err){
              console.log("error occured while connecting");
          }
          else{
              console.log("connection created with Mysql successfully");
          }
       });
      return connection2
}