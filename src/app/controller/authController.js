const { promisify } = require("util");
const jwt = require("jsonwebtoken");
var mongoose = require('mongoose');
const websitesModel = require('../models/websites');
const websites2Model = require('../models/websites2');
// require('../models/websites')
// var websitesModel = mongoose.model('Websites');
const priorityRulesModel = require('../models/priority-rules');
// const priorityRulesModel= require('../models/priority-rules');
// var priorityRulesModel = mongoose.model('Priority_rules');
const AppError = require("../../utils/appError");
// const JWT_SECRET = '0d7c5c5f-768c-4d98-8900-13aadaa21937';
// const JWT_EXPIRES_IN = '24h';
var encrypt = require('../../libs/encrypt');
const cipher = require('../../api/comman/auth/ciperHelper');
const bcrypt = require("bcryptjs");
const emailvalidator = require('email-validator');
var validator = require('../../middleware/validator');
var dbController = require('./dbController');
var commanController = require('./commanController');
var trim = require('trim');
const { exitCode, exit } = require("process");
const { count } = require("console");

const createToken = tenant_id => {
  return jwt.sign(
    {
      tenant_id,
    },
    process.env.JWT_SECRET,
    {
      expiresIn: process.env.JWT_EXPIRES_IN,
    },
  );
};

exports.login = async (req, res, next) => {
  try {
    // 1) check if email and password exist
    if (!req.body.email || !req.body.password) {
      res.json({
        status: 404,
        message: "fail, Please provide email or password"
      })
    } else {
      // 2) check if user exist and password is correct
      var email = trim(req.body.email)
      var password = trim(req.body.password)
      const user = await websitesModel.findOne({
        email,
        password
      });

      if (!user) {
        res.json({
          status: 401,
          message: "fail, Email or Password is wrong"
        })
      } else {
        // 3) All correct, send jwt to client
        const token = createToken(user.tenant_id);
        // console.log("@@@---",req.headers.authorization)
        // Remove the password from the output
        user.password = undefined;
        user.passwordHash = undefined;
        user.salt = undefined;
        res.status(200).json({
          status: "success",
          token,
          data: {
            user,
          },
        });
      }
    }
  } catch (error) {
    // console.log(`Error: ${error.message}`)
    return res.status(500).json({
      error: error.message
    })
  }
};

exports.saveWebsites = async (req, res, next) => {
  try {
    if(req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '') &&
      req.body.courier_code && (req.body.courier_code !== undefined || req.body.courier_code !== 'undefined' || req.body.courier_code !== '') && 
      req.body.email && (req.body.email !== undefined || req.body.email !== 'undefined' || req.body.email !== '') && 
      req.body.userName && (req.body.userName !== undefined || req.body.userName !== 'undefined' || req.body.userName !== '') &&
      req.body.password && (req.body.password !== undefined || req.body.password !== 'undefined' || req.body.password !== '')){

        var tenant_id = trim(req.body.tenant_id)
        var courier_code = trim(req.body.courier_code)
        var email = trim(req.body.email)
        var userName = trim(req.body.userName)
        var password = trim(req.body.password)
        if (emailvalidator.validate(email)) {
          const { salt, passwordHash } = cipher.saltHashPassword(password);
          websitesModel.findOneAndUpdate(
            {
              tenant_id: tenant_id,
              courier_code: courier_code,
              email: email,
            },
            {
              $set: {
                userName: userName,
                password: password,
                passwordHash: passwordHash,
                salt: salt
              }
            },
            { upsert: true, new: true },
            function (err, websites) {
              if (err) {
                res.json({
                  status: 400,
                  message: 'Some Error Occured During Creation.'
                });
              } else {
                // console.log(websites);
                // const token = createToken(websites.tenant_id);
                websites.password = undefined;
                websites.passwordHash = undefined;
                websites.salt = undefined;
                res.status(200).json({
                  status: "success",
                  data: {
                    websites,
                  },
                });
              }
            });
    
        } else {
          res.json({
            status: 400,
            message: 'Invalid Email'
    
          })
        }

      }else{
        res.json({
          status: 400,
          message: 'All fields are required..'
  
        })
      }

  } catch (error) {
    // console.log(`Error: ${error.message}`)
    return res.status(500).json({
      error: error.message
    })
  }
};

exports.savePriorityRules = async (req, res, next) => {
  if (req.body.tenant_id == undefined) {
    res.json({
      message: 'Required all fields....'
    })
  } else {
    if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')) {
      var platform = trim(req.body.platform)
      if (req.body.pay_type != undefined) {
        if (req.body.rule_code == 'WZ') {
          if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
            res.json({
              message: 'Validation: Required zone, weight and priority...'
            })
          } else {
            var tenant_id = trim(req.body.tenant_id)
            var pay_type = trim(req.body.pay_type)
            var rule_code = trim(req.body.rule_code)
            var zone = trim(req.body.zone)
            var weight = trim(req.body.weight)
            var arr = weight.split("-")
            var from = trim(arr[0])
            var to = trim(arr[1])
            // console.log("from----",from)
            // console.log("to----",to)

            var newlist = [];

            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                }
          })


            var existsData = []
            if (req.body._id) {
              var _id = trim(req.body._id)
              existsData = await priorityRulesModel.find({
                _id: _id
              })
              if (existsData.length != 0) {
                try {
                  priorityRulesModel.findOneAndUpdate(
                    {
                      _id: _id                                       //check this related data in collection
                    },
                    {
                      $set: {
                        tenant_id: tenant_id,
                        rule_code: rule_code,
                        weight: weight,
                        pay_type: pay_type,
                        zone: zone,
                        from: from,
                        to: to,                                   // update this are column value
                        list: newlist
                      }
                    },
                    { upsert: true ,new: true},  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one.
                    function (err, priorityRules) {
                      if (err) {
                        // console.log(err);
                        res.json({
                          status: 400,
                          message: 'Some Error Occured During Creation.'
                        })
                      } else {
                        res.json({
                          status: 200,
                          data: priorityRules._id,
                          message: 'save successfully'
                        });
                      }
                    });

                } catch (error) {
                  // console.log(`Error: ${error.message}`)
                  return res.status(500).json({
                    error: err.message
                  })
                }
              }
            } else {
              var newlist = [];
              req.body.list.forEach(function(data){
                  if(data.courier_code){
                      newlist.push({
                        courier_code: data.courier_code,
                      })
                    }
              })
              try {
                var priorityData = new priorityRulesModel({
                  tenant_id: tenant_id,
                  rule_code: rule_code,
                  weight: weight,
                  pay_type: pay_type,
                  zone: zone,
                  from: from,
                  to: to,                                   // update this are column value
                  list: newlist
                });
                priorityData.save(function (err, priorityRules) {
                  if (err) {
                    res.json({
                      status: 400,
                      message: 'Some Error Occured During Creation.'
                    })
                  } else {
                    res.json({
                      status: 200,
                      data: priorityRules._id,
                      message: 'save successfully'
                    });
                  }
                });
              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }

          }
        } else if (req.body.rule_code == 'ZM') {
          if (req.body.zone == undefined || req.body.list == undefined) {
            res.json({
              message: 'Validation: Required zone and priority...'
            })
          } else {
            var tenant_id = trim(req.body.tenant_id)
            var pay_type = trim(req.body.pay_type)
            var rule_code = trim(req.body.rule_code)
            var zone = trim(req.body.zone)
            var newlist = [];
            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                }
          })
            var existsData = []
            if (req.body._id) {
              var _id = trim(req.body._id)
              existsData = await priorityRulesModel.find({
                _id: _id
              })
              if (existsData.length != 0) {
                try {
                  priorityRulesModel.findOneAndUpdate(
                    {
                      _id: _id              //check this related data in collection
                    },
                    {
                      $set: {
                        tenant_id: tenant_id,
                        rule_code: rule_code,
                        weight: 'null',
                        pay_type: pay_type,
                        zone: zone,
                        from: 'null',
                        to: 'null', // update this are column value                      
                        list: newlist
                      }
                    },
                    { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                    function (err, priorityRules) {
                      if (err) {
                        // console.log(err);
                        res.json({
                          status: 400,
                          message: 'Some Error Occured During Creation.'
                        })
                      } else {
                        // console.log(priorityRules);
                        res.json({
                          status: 200,
                          data: priorityRules._id,
                          message: 'save successfully'
                        });
                      }
                    });

                } catch (error) {
                  // console.log(`Error: ${error.message}`)
                  return res.status(500).json({
                    error: err.message
                  })
                }
              }
            } else {
              var newlist = [];
              req.body.list.forEach(function(data){
                  if(data.courier_code){
                      newlist.push({
                        courier_code: data.courier_code,
                      })
                    } 
              })
              try {
                var priorityData = new priorityRulesModel({
                  tenant_id: tenant_id,
                  rule_code: rule_code,
                  pay_type: pay_type,
                  zone: zone,                         // update this are column value
                  list: newlist
                });
                priorityData.save(function (err, priorityRules) {
                  if (err) {
                    res.json({
                      status: 400,
                      message: 'Some Error Occured During Creation.'
                    })
                  } else {
                    res.json({
                      status: 200,
                      data: priorityRules._id,
                      message: 'save successfully'
                    });
                  }
                });
              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }

          }
        } else if (req.body.rule_code == 'WM') {
          if (req.body.weight == undefined || req.body.list == undefined) {
            res.json({
              message: 'Validation: Required weight and priority...'
            })
          } else {
            var tenant_id = trim(req.body.tenant_id)
            var pay_type = trim(req.body.pay_type)
            var rule_code = trim(req.body.rule_code)
            var weight = trim(req.body.weight)
            var arr = weight.split("-");
            var from = trim(arr[0])
            var to = trim(arr[1])
            // console.log("from----",from)
            // console.log("to----",to)
            var newlist = [];
            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                }
          })
            var existsData = []
            if (req.body._id) {
              var _id = trim(req.body._id)
              existsData = await priorityRulesModel.find({
                _id: _id
              })
              if (existsData.length != 0) {
                try {
                  priorityRulesModel.findOneAndUpdate(
                    {
                      _id: _id                //check this related data in collection
                    },
                    {
                      $set: {
                        tenant_id: tenant_id,
                        rule_code: rule_code,
                        weight: weight,
                        zone: 'null',
                        pay_type: pay_type,
                        from: from,
                        to: to,                                   // update this are column value                      
                        list: newlist
                      }
                    },
                    { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                    function (err, priorityRules) {
                      if (err) {
                        // console.log(err);
                        res.json({
                          status: 400,
                          message: 'Some Error Occured During Creation.'
                        })
                      } else {
                        // console.log(priorityRules);
                        res.json({
                          status: 200,
                          data: priorityRules._id,
                          message: 'save successfully'
                        });
                      }
                    });

                } catch (error) {
                  // console.log(`Error: ${error.message}`)
                  return res.status(500).json({
                    error: err.message
                  })
                }
              }
            } else {
              var newlist = [];
              req.body.list.forEach(function(data){
                  if(data.courier_code){
                      newlist.push({
                        courier_code: data.courier_code,
                      })
                    } 
              })
              try {
                var priorityData = new priorityRulesModel({
                  tenant_id: tenant_id,
                  rule_code: rule_code,
                  weight: weight,
                  pay_type: pay_type,
                  from: from,
                  to: to,                        // update this are column value
                  list: newlist
                });
                priorityData.save(function (err, priorityRules) {
                  if (err) {
                    res.json({
                      status: 400,
                      message: 'Some Error Occured During Creation.'
                    })
                  } else {
                    res.json({
                      status: 200,
                      data: priorityRules._id,
                      message: 'save successfully'
                    });
                  }
                });
              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }

          }
        } else if (req.body.list) {
          var tenant_id = trim(req.body.tenant_id)
          var pay_type = trim(req.body.pay_type)
          var newlist = [];
          console.log("req.body.list---",req.body.list)
          req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                } 
          })
                 
          var existsData = []
          if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await priorityRulesModel.find({
              _id: _id
            })
            if (existsData.length != 0) {
              try {
                priorityRulesModel.findOneAndUpdate(
                  {
                    _id: _id
                  },
                  {
                    $set: {
                      tenant_id: tenant_id,
                      pay_type: pay_type,
                      rule_code: 'default_rule',
                      weight: 'null',
                      zone: 'null',
                      from: 'null',
                      to: 'null',
                      list: newlist                           // update this are column value
                    }
                  },
                  { upsert: true,new:true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                  function (err, priorityRules) {
                    if (err) {
                      // console.log(err);
                      res.json({
                        status: 400,
                        message: 'Some Error Occured During Creation.'
                      })
                    } else {
                      // res.json({
                      //   status: 200,
                      //   message: 'save successfully'
                      // });
                      if(platform.toUpperCase() == 'V1'){
                        if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                          var database = trim(req.body.database)
                        var list = req.body.list
                        var newlist = [];
                        req.body.list.forEach(function(data){
                          if(data.courier_code){
                              newlist.push({
                                courier_code: data.courier_code,
                              })
                            } 
                      })
                        var courier_code = []
                        var count = 0
                        newlist.forEach(async function (element) {
                          count++
                          await courier_code.push({
                            courier: element.courier_code,
                            priority: count
                          })
                          
                        })
                        dbController.getMysqlDBClient(database).then(function (connection2) {
                          var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                          connection2.query(sql, (error, results) => {
                            if (error) {
                              return res.status(400).json({
                                message: 'Bad Request',
                              })
                            } else {
                              // console.log("results----",results)
                              courier_code.forEach(async function (element1) {
                                var courier = element1.courier
                                var priority = element1.priority
                                if(courier){
                                  let el = await results.find(itm => itm.courier_code === courier);                             
                                  if(el){
                                    var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' AND is_active = 1";
                                    connection2.query(sql2, (error, results) => {
                                      if (error) {
                                        return res.status(400).json({
                                          message: 'Bad Request',
                                        })
                                      } else {
                                        // console.log("results----",results)                              
                                      }
                                    }) 
                                  }
                              }
                              let el3 = await results.some(item => item.courier_code === courier)
                              // console.log("el3----",el3)
                              if(el3 == false){
                                // console.log("courier----",courier)
                                var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' ";
                                connection2.query(sql3, (error, results) => {
                                  if (error) {
                                    return res.status(400).json({
                                      message: 'Bad Request',
                                    })
                                  } else {
                                    // console.log("results----",results)                              
                                  }
                                })
                              }
                            })
                            results.forEach(async function(data){
                              var resultCourier = data.courier_code
                              let el2 = await courier_code.some(item => item.courier === resultCourier)
                              // console.log("el2---",el2)                             
                              if(el2 == false){
                                var sql2 = "UPDATE courier_master SET is_active = "+ 0 +" WHERE courier_code = '"+ resultCourier +"' AND is_active = 1";
                                  // console.log("sql2---",sql2)
                                connection2.query(sql2, (error, results) => {
                                    if (error) {
                                      return res.status(400).json({
                                        message: 'Bad Request',
                                      })
                                    } else {
                                      // console.log("results----",results)                              
                                    }
                                  })
                              } else if(el2 == true){

                              }
                            })

                           
                            }
                          })                          
                        })
                      }else{
                        res.json({
                          message: 'Database is required....'
                        })
                      }
                      }else if(platform.toUpperCase() == 'V2'){
                        var list = req.body.list
                        var newlist = [];
                        req.body.list.forEach(function(data){
                          if(data.courier_code){
                              newlist.push({
                                courier_code: data.courier_code,
                              })
                            } 
                      })
                        let result = newlist.map(a => a.courier_code);
                        // console.log("result---",result)
                        try{
                          websites2Model.findOneAndUpdate(
                            {
                              _id: tenant_id
                            },
                            {
                              $set: {
                                enabled_couriers: result                          // update this are column value
                              }
                            },
                            { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                            function (err, priorityRules) {
                              if (err) {
                                // console.log(err);
                                res.json({
                                  status: 400,
                                  message: 'Some Error Occured During Creation.'
                                })
                              } else {
                                // console.log(priorityRules);
                                // res.json({
                                //   status: 200,
                                //   message: 'save successfully'
                                // });
                              }
                            });
                        }catch (error) {
                          // console.log(`Error: ${error.message}`)
                          return res.status(500).json({
                            error: err.message
                          })
                        }
                      }
                      res.json({
                        status: 200,
                        data: priorityRules._id,
                        message: 'save successfully'
                      });
                    }
                    
                  });

              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }
          } else {
            var newlist = [];
            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                } 
          })
            try {
              var priorityData = new priorityRulesModel({
                tenant_id: tenant_id,
                pay_type: pay_type,
                rule_code: 'default_rule',
                list: newlist
              });
              priorityData.save(function (err, priorityRules) {
                if (err) {
                  res.json({
                    status: 400,
                    message: 'Some Error Occured During Creation.'
                  })
                } else {
                  // res.json({
                  //   status: 200,
                  //   message: 'save successfully'
                  // });
                  if(platform.toUpperCase() == 'V1'){
                    if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                      var database = trim(req.body.database)
                    var list = req.body.list
                    var newlist = [];
                    req.body.list.forEach(function(data){
                      if(data.courier_code){
                          newlist.push({
                            courier_code: data.courier_code,
                          })
                        } 
                  })
                    var courier_code = []
                    var count = 0
                    newlist.forEach(async function (element) {
                      count++
                     await courier_code.push({
                        courier: element.courier_code,
                        priority: count
                      })
                      
                    })
                    dbController.getMysqlDBClient(database).then(function (connection2) {
                      var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                      connection2.query(sql, (error, results) => {
                        if (error) {
                          return res.status(400).json({
                            message: 'Bad Request',
                          })
                        } else {
                          // console.log("results----",results)
                          courier_code.forEach(async function (element1) {
                            var courier = element1.courier
                            var priority = element1.priority
                            if(courier){
                              let el = await results.find(itm => itm.courier_code === courier);                             
                              if(el){
                                var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' AND is_active = 1";
                                connection2.query(sql2, (error, results) => {
                                  if (error) {
                                    return res.status(400).json({
                                      message: 'Bad Request',
                                    })
                                  } else {
                                    // console.log("results----",results)                              
                                  }
                                }) 
                              }
                          }
                          let el3 = await results.some(item => item.courier_code === courier)
                          // console.log("el3----",el3)
                          if(el3 == false){
                            // console.log("courier----",courier)
                            var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' ";
                            connection2.query(sql3, (error, results) => {
                              if (error) {
                                return res.status(400).json({
                                  message: 'Bad Request',
                                })
                              } else {
                                // console.log("results----",results)                              
                              }
                            })
                          }
                        })
                        results.forEach(async function(data){
                          var resultCourier = data.courier_code
                          let el2 = await courier_code.some(item => item.courier === resultCourier)
                          // console.log("el2---",el2)                             
                          if(el2 == false){
                            var sql2 = "UPDATE courier_master SET is_active = "+ 0 +" WHERE courier_code = '"+ resultCourier +"' AND is_active = 1";
                              // console.log("sql2---",sql2)
                            connection2.query(sql2, (error, results) => {
                                if (error) {
                                  return res.status(400).json({
                                    message: 'Bad Request',
                                  })
                                } else {
                                  // console.log("results----",results)                              
                                }
                              })
                          } else if(el2 == true){

                          }
                        })

                       
                        }
                      })                          
                    })
                  }else{
                    res.json({
                      message: 'Database is required....'
                    })
                  }
                  }else if(platform.toUpperCase() == 'V2'){
                    var list = req.body.list
                    var newlist = [];
                    req.body.list.forEach(function(data){
                      if(data.courier_code){
                          newlist.push({
                            courier_code: data.courier_code,
                          })
                        } 
                  })
                    let result = newlist.map(a => a.courier_code);
                    // console.log("result---",result)
                    try{
                      websites2Model.findOneAndUpdate(
                        {
                          _id: tenant_id
                        },
                        {
                          $set: {
                            enabled_couriers: result                          // update this are column value
                          }
                        },
                        { upsert: true,new:true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                        function (err, priorityRules) {
                          if (err) {
                            // console.log(err);
                            res.json({
                              status: 400,
                              message: 'Some Error Occured During Creation.'
                            })
                          } else {
                            // console.log(priorityRules);
                            // res.json({
                            //   status: 200,
                            //   message: 'save successfully'
                            // });
                          }
                        });
                    }catch (error) {
                      // console.log(`Error: ${error.message}`)
                      return res.status(500).json({
                        error: err.message
                      })
                    }
                  }
                  res.json({
                    status: 200,
                    data: priorityRules._id,
                    message: 'save successfully'
                  });
                }
                
              });
            } catch (error) {
              // console.log(`Error: ${error.message}`)
              return res.status(500).json({
                error: err.message
              })
            }
          }
        } else {
          res.json({
            message: 'Required all fields....'
          })
        }
      } else if (req.body.pay_type == undefined) {
        if (req.body.rule_code == 'WZ') {
          if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
            res.json({
              message: 'Validation: Required zone, weight and priority...'
            })
          } else {
            var tenant_id = trim(req.body.tenant_id)
            var rule_code = trim(req.body.rule_code)
            var zone = trim(req.body.zone)
            var weight = trim(req.body.weight)
            var arr = weight.split("-");
            var from = trim(arr[0])
            var to = trim(arr[1])
            // console.log("from----",from)
            // console.log("to----",to)
            var newlist = [];
            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                } 
          })
            var existsData = []
            if (req.body._id) {
              var _id = trim(req.body._id)
              existsData = await priorityRulesModel.find({
                _id: _id
              })
              if (existsData.length != 0) {
                try {
                  priorityRulesModel.findOneAndUpdate(
                    {
                      _id: _id            //check this related data in collection
                    },
                    {
                      $set: {
                        tenant_id: tenant_id,
                        rule_code: rule_code,
                        weight: weight,
                        pay_type: 'BOTH',
                        zone: zone,
                        from: from,
                        to: to,                                // update this are column value                     
                        list: newlist
                      }
                    },
                    { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                    function (err, priorityRules) {
                      if (err) {
                        // console.log(err);
                        res.json({
                          status: 400,
                          message: 'Some Error Occured During Creation.'
                        })
                      } else {
                        // console.log(priorityRules);
                        res.json({
                          status: 200,
                          data: priorityRules._id,
                          message: 'save successfully'
                        });
                      }
                    });

                } catch (error) {
                  // console.log(`Error: ${error.message}`)
                  return res.status(500).json({
                    error: err.message
                  })
                }
              }
            } else {
              var newlist = [];
              req.body.list.forEach(function(data){
                  if(data.courier_code){
                      newlist.push({
                        courier_code: data.courier_code,
                      })
                    } 
              })
              try {
                var priorityData = new priorityRulesModel({
                  tenant_id: tenant_id,
                  rule_code: rule_code,
                  weight: weight,
                  pay_type: 'BOTH',
                  zone: zone,
                  from: from,
                  to: to,                                // update this are column value                     
                  list: newlist
                });
                priorityData.save(function (err, priorityRules) {
                  if (err) {
                    res.json({
                      status: 400,
                      message: 'Some Error Occured During Creation.'
                    })
                  } else {
                    res.json({
                      status: 200,
                      data: priorityRules._id,
                      message: 'save successfully'
                    });
                  }
                });
              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }

          }
        } else if (req.body.rule_code == 'ZM') {
          if (req.body.zone == undefined || req.body.list == undefined) {
            res.json({
              message: 'Validation: Required zone and priority...'
            })
          } else {
            var tenant_id = trim(req.body.tenant_id)
            var rule_code = trim(req.body.rule_code)
            var zone = trim(req.body.zone)
            var newlist = [];
            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                } 
          })
            var existsData = []
            if (req.body._id) {
              var _id = trim(req.body._id)
              existsData = await priorityRulesModel.find({
                _id: _id
              })
              if (existsData.length != 0) {
                try {
                  priorityRulesModel.findOneAndUpdate(
                    {
                      _id: _id                    //check this related data in collection
                    },
                    {
                      $set: {
                        tenant_id: tenant_id,
                        rule_code: rule_code,
                        weight: 'null',
                        pay_type: 'BOTH',
                        zone: zone,
                        from: 'null',
                        to: 'null',                                // update this are column value                     
                        list: newlist
                      }
                    },
                    { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                    function (err, priorityRules) {
                      if (err) {
                        // console.log(err);
                        res.json({
                          status: 400,
                          message: 'Some Error Occured During Creation.'
                        })
                      } else {
                        // console.log(priorityRules);
                        res.json({
                          status: 200,
                          data: priorityRules._id,
                          message: 'save successfully'
                        });
                      }
                    });

                } catch (error) {
                  // console.log(`Error: ${error.message}`)
                  return res.status(500).json({
                    error: err.message
                  })
                }
              }
            } else {
              var newlist = [];
              req.body.list.forEach(function(data){
                  if(data.courier_code){
                      newlist.push({
                        courier_code: data.courier_code,
                      })
                    } 
              })
              try {
                var priorityData = new priorityRulesModel({
                  tenant_id: tenant_id,
                  rule_code: rule_code,
                  pay_type: 'BOTH',
                  zone: zone,                                  // update this are column value                     
                  list: newlist
                });
                priorityData.save(function (err, priorityRules) {
                  if (err) {
                    res.json({
                      status: 400,
                      message: 'Some Error Occured During Creation.'
                    })
                  } else {
                    res.json({
                      status: 200,
                      data: priorityRules._id,
                      message: 'save successfully'
                    });
                  }
                });
              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }

          }
        } else if (req.body.rule_code == 'WM') {
          if (req.body.weight == undefined || req.body.list == undefined) {
            res.json({
              message: 'Validation: Required weight and priority...'
            })
          } else {
            var tenant_id = trim(req.body.tenant_id)
            var rule_code = trim(req.body.rule_code)
            var weight = trim(req.body.weight)
            var arr = weight.split("-");
            var from = trim(arr[0])
            var to = trim(arr[1])
            // console.log("from----",from)
            // console.log("to----",to)
            var newlist = [];
            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                } 
          })
            var existsData = []
            if (req.body._id) {
              var _id = trim(req.body._id)
              existsData = await priorityRulesModel.find({
                _id: _id
              })
              if (existsData.length != 0) {
                try {
                  priorityRulesModel.findOneAndUpdate(
                    {
                      _id: _id                 //check this related data in collection
                    },
                    {
                      $set: {
                        tenant_id: tenant_id,
                        rule_code: rule_code,
                        weight: weight,
                        pay_type: 'BOTH',
                        zone: 'null',
                        from: from,
                        to: to,                                // update this are column value                     
                        list: newlist
                      }
                    },
                    { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                    function (err, priorityRules) {
                      if (err) {
                        // console.log(err);
                        res.json({
                          status: 400,
                          message: 'Some Error Occured During Creation.'
                        })
                      } else {
                        // console.log(priorityRules);
                        res.json({
                          status: 200,
                          data: priorityRules._id,
                          message: 'save successfully'
                        });
                      }
                    });

                } catch (error) {
                  // console.log(`Error: ${error.message}`)
                  return res.status(500).json({
                    error: err.message
                  })
                }
              }
            } else {
              var newlist = [];
              req.body.list.forEach(function(data){
                  if(data.courier_code){
                      newlist.push({
                        courier_code: data.courier_code,
                      })
                    } 
              })
              try {
                var priorityData = new priorityRulesModel({
                  tenant_id: tenant_id,
                  rule_code: rule_code,
                  weight: weight,
                  pay_type: 'BOTH',
                  from: from,
                  to: to,
                  list: newlist
                });
                priorityData.save(function (err, result) {
                  if (err) {
                    res.json({
                      status: 400,
                      message: 'Some Error Occured During Creation.'
                    })
                  } else {
                    res.json({
                      status: 200,
                      data: priorityRules._id,
                      message: 'save successfully'
                    });
                  }
                });
              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }

          }
        } else if (req.body.list) {
          var tenant_id = trim(req.body.tenant_id)
          var newlist = [];
          req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                } 
          })
          var existsData = []
          if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await priorityRulesModel.find({
              _id: _id
            })
            if (existsData.length != 0) {
              try {
                priorityRulesModel.findOneAndUpdate(
                  {
                    _id: _id
                  },
                  {
                    $set: {
                      tenant_id: tenant_id,
                      rule_code: 'default_rule',
                      weight: 'null',
                      pay_type: 'BOTH',
                      zone: 'null',
                      from: 'null',
                      to: 'null',                                // update this are column value                     
                      list: newlist
                    }
                  },
                  { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                  function (err, priorityRules) {
                    if (err) {
                      // console.log(err);
                      res.json({
                        status: 400,
                        message: 'Some Error Occured During Creation.'
                      })
                    } else {
                      // console.log(priorityRules);
                      // res.json({
                      //   status: 200,
                      //   message: 'save successfully'
                      // });
                      if(platform.toUpperCase() == 'V1'){
                        if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                          var database = trim(req.body.database)
                        var list = req.body.list
                          var newlist = [];
                          req.body.list.forEach(function(data){
                              if(data.courier_code){
                                  newlist.push({
                                    courier_code: data.courier_code,
                                  })
                                } 
                          })
                        var courier_code = []
                        var count = 0
                        newlist.forEach(async function (element) {
                          count++
                          await courier_code.push({
                            courier: element.courier_code,
                            priority: count
                          })
                          
                        })
                        dbController.getMysqlDBClient(database).then(function (connection2) {
                          var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                          connection2.query(sql, (error, results) => {
                            if (error) {
                              return res.status(400).json({
                                message: 'Bad Request',
                              })
                            } else {
                              // console.log("results----",results)
                              courier_code.forEach(async function (element1) {
                                var courier = element1.courier
                                var priority = element1.priority
                                if(courier){
                                  let el = await results.find(itm => itm.courier_code === courier);                             
                                  if(el){
                                    var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' AND is_active = 1";
                                    connection2.query(sql2, (error, results) => {
                                      if (error) {
                                        return res.status(400).json({
                                          message: 'Bad Request',
                                        })
                                      } else {
                                        // console.log("results----",results)                              
                                      }
                                    }) 
                                  }
                              }
                              let el3 = await results.some(item => item.courier_code === courier)
                              // console.log("el3----",el3)
                              if(el3 == false){
                                // console.log("courier----",courier)
                                var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' ";
                                connection2.query(sql3, (error, results) => {
                                  if (error) {
                                    return res.status(400).json({
                                      message: 'Bad Request',
                                    })
                                  } else {
                                    // console.log("results----",results)                              
                                  }
                                })
                              }
                            })
                            results.forEach(async function(data){
                              var resultCourier = data.courier_code
                              let el2 = await courier_code.some(item => item.courier === resultCourier)
                              // console.log("el2---",el2)                             
                              if(el2 == false){
                                var sql2 = "UPDATE courier_master SET is_active = "+ 0 +" WHERE courier_code = '"+ resultCourier +"' AND is_active = 1";
                                  // console.log("sql2---",sql2)
                                connection2.query(sql2, (error, results) => {
                                    if (error) {
                                      return res.status(400).json({
                                        message: 'Bad Request',
                                      })
                                    } else {
                                      // console.log("results----",results)                              
                                    }
                                  })
                              } else if(el2 == true){
    
                              }
                            })
    
                           
                            }
                          })                          
                        })
                      }else{
                        res.json({
                          message: 'Database is required....'
                        })
                      }
                      }else if(platform.toUpperCase() == 'V2'){
                        var list = req.body.list
                        var newlist = [];
                        req.body.list.forEach(function(data){
                          if(data.courier_code){
                              newlist.push({
                                courier_code: data.courier_code,
                              })
                            } 
                      })
                        let result = newlist.map(a => a.courier_code);
                        // console.log("result---",result)
                        try{
                          websites2Model.findOneAndUpdate(
                            {
                              _id: tenant_id
                            },
                            {
                              $set: {
                                enabled_couriers: result                          // update this are column value
                              }
                            },
                            { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                            function (err, priorityRules) {
                              if (err) {
                                // console.log(err);
                                res.json({
                                  status: 400,
                                  message: 'Some Error Occured During Creation.'
                                })
                              } else {
                                // console.log(priorityRules);
                                // res.json({
                                //   status: 200,
                                //   message: 'save successfully'
                                // });
                              }
                            });
                        }catch (error) {
                          // console.log(`Error: ${error.message}`)
                          return res.status(500).json({
                            error: err.message
                          })
                        }
                      }
                      res.json({
                        status: 200,
                        data: priorityRules._id,
                        message: 'save successfully'
                      });
                    } 
                  });

              } catch (error) {
                // console.log(`Error: ${error.message}`)
                return res.status(500).json({
                  error: err.message
                })
              }
            }
          } else {
            var newlist = [];
            req.body.list.forEach(function(data){
              if(data.courier_code){
                  newlist.push({
                    courier_code: data.courier_code,
                  })
                } 
          })
            try {
              var priorityData = new priorityRulesModel({
                tenant_id: tenant_id,
                pay_type: 'BOTH',
                rule_code: 'default_rule',
                list: newlist
              });
              priorityData.save(function (err, priorityRules) {
                if (err) {
                  res.json({
                    status: 400,
                    message: 'Some Error Occured During Creation.'
                  })
                } else {
                  // res.json({
                  //   status: 200,
                  //   message: 'save successfully'
                  // });
                  if(platform.toUpperCase() == 'V1'){
                    if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                      var database = trim(req.body.database)
                    var list = req.body.list
                      var newlist = [];
                      req.body.list.forEach(function(data){
                          if(data.courier_code){
                              newlist.push({
                                courier_code: data.courier_code,
                              })
                            } 
                      })
                    var courier_code = []
                    var count = 0
                    newlist.forEach(async function (element) {
                      count++
                      await courier_code.push({
                        courier: element.courier_code,
                        priority: count
                      })
                      
                    })
                    dbController.getMysqlDBClient(database).then(function (connection2) {
                      var sql = "select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc";
                      connection2.query(sql, (error, results) => {
                        if (error) {
                          return res.status(400).json({
                            message: 'Bad Request',
                          })
                        } else {
                          // console.log("results----",results)
                          courier_code.forEach(async function (element1) {
                            var courier = element1.courier
                            var priority = element1.priority
                            if(courier){
                              let el = await results.find(itm => itm.courier_code === courier);                             
                              if(el){
                                var sql2 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' AND is_active = 1";
                                connection2.query(sql2, (error, results) => {
                                  if (error) {
                                    return res.status(400).json({
                                      message: 'Bad Request',
                                    })
                                  } else {
                                    // console.log("results----",results)                              
                                  }
                                }) 
                              }
                          }
                          let el3 = await results.some(item => item.courier_code === courier)
                          // console.log("el3----",el3)
                          if(el3 == false){
                            // console.log("courier----",courier)
                            var sql3 = "UPDATE courier_master SET priority = " + priority + ", is_active = "+ 1 +" WHERE courier_code = '"+courier+"' ";
                            connection2.query(sql3, (error, results) => {
                              if (error) {
                                return res.status(400).json({
                                  message: 'Bad Request',
                                })
                              } else {
                                // console.log("results----",results)                              
                              }
                            })
                          }
                        })
                        results.forEach(async function(data){
                          var resultCourier = data.courier_code
                          let el2 = await courier_code.some(item => item.courier === resultCourier)
                          // console.log("el2---",el2)                             
                          if(el2 == false){
                            var sql2 = "UPDATE courier_master SET is_active = "+ 0 +" WHERE courier_code = '"+ resultCourier +"' AND is_active = 1";
                              // console.log("sql2---",sql2)
                            connection2.query(sql2, (error, results) => {
                                if (error) {
                                  return res.status(400).json({
                                    message: 'Bad Request',
                                  })
                                } else {
                                  // console.log("results----",results)                              
                                }
                              })
                          } else if(el2 == true){

                          }
                        })

                       
                        }
                      })                          
                    })
                  }else{
                    res.json({
                      message: 'Database is required....'
                    })
                  }
                  }else if(platform.toUpperCase() == 'V2'){
                    var list = req.body.list
                    var newlist = [];
                    req.body.list.forEach(function(data){
                      if(data.courier_code){
                          newlist.push({
                            courier_code: data.courier_code,
                          })
                        } 
                  })
                    let result = newlist.map(a => a.courier_code);
                    // console.log("result---",result)
                    try{
                      websites2Model.findOneAndUpdate(
                        {
                          _id: tenant_id
                        },
                        {
                          $set: {
                            enabled_couriers: result                          // update this are column value
                          }
                        },
                        { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                        function (err, priorityRules) {
                          if (err) {
                            // console.log(err);
                            res.json({
                              status: 400,
                              message: 'Some Error Occured During Creation.'
                            })
                          } else {
                            // console.log(priorityRules);
                            // res.json({
                            //   status: 200,
                            //   message: 'save successfully'
                            // });
                          }
                        });
                    }catch (error) {
                      // console.log(`Error: ${error.message}`)
                      return res.status(500).json({
                        error: err.message
                      })
                    }
                  }
                  res.json({
                    status: 200,
                    data: priorityRules._id,
                    message: 'save successfully'
                  });
                }
                
              });
            } catch (error) {
              // console.log(`Error: ${error.message}`)
              return res.status(500).json({
                error: err.message
              })
            }
          }
          // res.json({
          //   status: 200,
          //   message: 'save successfully'
          // });
        } else {
          res.json({
            message: 'Required all fields....'
          })
        }
      }
    } else {
      res.json({
        message: 'Platfrom is Required....'
      })
    }
  }
}

/*
    This route used for get v1(using client db in mysql) and v2(defaultly connected mongodb) platforms couriers data ....(default priority rule).
    If changes in defaults priority rule (if, is_active = 0 or deleted couriers) then this changes are updated on related couriers priority rules.
*/
exports.getCourierData = async (req, res, next) => {
  // console.log("req.body---",req.body)
  if (req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')) {
    // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
      if (req.body.platform && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')) {
        var tenant_id = trim(req.body.tenant_id)
        var platform = trim(req.body.platform) 
        if (platform.toUpperCase() == 'V1') {
          if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
            var database = trim(req.body.database)
            var list = [];
            var sql = 'select * from courier_master where is_active=1 AND priority IS NOT NULL order by priority asc';
            dbController.getMysqlDBClient(database.toString()).then(function (connection2) {
              connection2.query(sql, (error, results) => {
                if (error) {
                  return res.status(400).json({
                    message: 'Bad Request',
                  })
                } else {                 
                  if (results != '') {
                    try {
                      for (let result of results) {
                        list.push({
                          // priority: result.priority,
                          courier_code: result.courier_code
                        })
                      }
                      priorityRulesModel.findOneAndUpdate(
                        {
                          tenant_id: tenant_id,
                          rule_code: 'default_rule'
                        },
                        {
                          $set: {
                            list: list                           // update this are column value
                          }
                        },
                        {new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                        async function (err, priorityRules) {
                          if (err) {
                            // console.log(err);
                            res.json({
                              status: 400,
                              message: 'Some Error Occured During Creation.'
                            })
                          } else {
                            var tenant_id = trim(req.body.tenant_id)
                            const priorityData = await priorityRulesModel.find({
                              tenant_id: tenant_id
                            })

                            for await (let element of priorityData) {
                              var tenant_id = element.tenant_id;
                              var lists = element.list
                              //  console.log("tenant_id---",tenant_id)
                              //  console.log("lists---",lists)
                              for await (let element1 of lists) {
                                // console.log("mongo.courier_code---",element1.courier_code)
                                
                                const isFound = list.some(element => {
                                  // console.log("mysql.courier_code--",element.courier_code)
                                  if (element.courier_code === element1.courier_code) {
                                    return true;
                                  }
                                });
                                // console.log("isFound---",isFound);
                                if (isFound === false) {
                                  let el = lists.find(itm => itm.courier_code === element1.courier_code);
                                  if (el)
                                    lists.splice(lists.indexOf(el), 1);
                                  priorityRulesModel.updateMany(
                                    {
                                      tenant_id: tenant_id,
                                    },
                                    {
                                      $set: {
                                        list: lists                           // update this are column value
                                      }
                                    },
                                    { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                                    async function (err, priorityRules) {
                                      if (err) {
                                        // console.log(err);
                                        res.json({
                                          status: 400,
                                          message: 'Some Error Occured During Updation.'
                                        })
                                      } else {
                                        // res.json({
                                        //   status: 200,
                                        //   message: 'success',
                                        // })
                                      }
                                    })
                                }
                              }
                            }
                            res.json({
                              status: 200,
                              message: 'success',
                              data: priorityRules
                            })
                          }
                        });
                    } catch (error) {
                      return res.status(500).json({
                        status: false,
                        message: 'Server Error'
                      })
                    }
                  } else {
                    return res.status(400).json({
                      data: results
                    })
                  }
                }
              })
  
            }).catch(function (err) {
              return res.status(500).json({
                status: false,
                error: err.message
              })
            })
          } else {
            res.json({
              status: 400,
              message: 'Validation: database is required...'
            })
          }
        } else if (platform.toUpperCase() == 'V2') {
          var v2list = [];
          var tenant_id = trim(req.body.tenant_id)
          try {
            const websites2Data = await websites2Model.find({
              _id: tenant_id
            })
            // console.log("websites2Data---",websites2Data)
            for (let data of websites2Data) {
              // console.log("data._id---",data._id)
              // console.log("enabled_couriers---",data.enabled_couriers)
              var count = 1
              for (let element of data.enabled_couriers) {
                // console.log("element---",element,count)
                v2list.push({
                  // priority: count,
                  courier_code: element
                })
                count++
              }
              // console.log("v2list----",v2list) 
            }
            // console.log("tenant_id@@---",tenant_id)
            var tenant_id = trim(req.body.tenant_id)
            priorityRulesModel.findOneAndUpdate(
              {
                tenant_id: tenant_id,
                rule_code: 'default_rule'
              },
              {
                $set: {
                  list: v2list                           // update this are column value
                }
              },
              {new: true, upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
              async function (err, priorityRules) {
                if (err) {
                  // console.log(err);
                  res.json({
                    status: 400,
                    message: 'Some Error Occured During Creation.'
                  })
                } else {
                  var tenant_id = trim(req.body.tenant_id)
                  // console.log("tenant_id$$---",tenant_id)
                  const priorityData = await priorityRulesModel.find({
                    tenant_id: tenant_id
                  })
                  // console.log("priorityData----",priorityData)
                  for await (let element of priorityData) {
                    var tenant_id = element.tenant_id;
                    var lists = element.list
                    //  console.log("tenant_id---",tenant_id)
                    //  console.log("lists---",lists)
                    for await (let element1 of lists) {
                      // console.log("mongo.courier_code---",element1.courier_code)

                      const isFound = v2list.some(element => {
                        // console.log("mysql.courier_code--",element.courier_code)
                        if (element.courier_code === element1.courier_code) {
                          return true;
                        }
                      });
                      // console.log("isFound---", isFound);
                      if (isFound === false) {
                        let el = lists.find(itm => itm.courier_code === element1.courier_code);
                        if (el)
                          lists.splice(lists.indexOf(el), 1);
                          var tenant_id = trim(req.body.tenant_id)
                        priorityRulesModel.updateMany(
                          {
                            tenant_id: tenant_id,
                          },
                          {
                            $set: {
                              list: lists                           // update this are column value
                            }
                          },
                          { upsert: true },  //upsert to create a new doc if none exists and new to return the new, updated document instead of the old one. 
                          async function (err, priorityRules) {
                            if (err) {
                              // console.log(err);
                              res.json({
                                status: 400,
                                message: 'Some Error Occured During Updation.'
                              })
                            } else {
                              // res.json({
                              //   status: 200,
                              //   message: 'success',
                              // })
                            }
                          })
                      }
                    }
                  }
                  
                  res.json({
                    status: 200,
                    message: 'success',
                    data: priorityRules
                  })
                }
              });
          } catch (error) {
            return res.status(500).json({
              status: false,
              error: error.message
            })
          }

        }
      } else {
        res.json({
          status: 400,
          message: 'Validation: Platform is required...'
        })
      }
    // } else {
    //   res.json({
    //     status: 400,
    //     message: 'Validation: database is required...'
    //   })
    // }
  } else {
    res.json({
      status: 400,
      message: 'Validation: Please select all field...'
    })
  }



}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

exports.getPriorityRules = async (req, res, next) => {
  await sleep(2000)
  try {
    if(req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '')){

      var tenant_id = trim(req.body.tenant_id)
      // console.log("tenant_id--",tenant_id)
      const priorityRules = await priorityRulesModel.find({
        tenant_id: tenant_id,                      //check tenant_id related data in collection
      });
      // console.log("priorityRules---", priorityRules)
      var activeData = []
      if(priorityRules.length == 1){
        console.log("inactive")
        activeData.push({
          status: 'Inactive'
        })
      }else if(priorityRules.length > 1){
        console.log("active")
        activeData.push({
          status: 'Active'
        })
      }
      if (priorityRules != '') {
        res.json({
          status: 200,
          message: 'success',
          data:priorityRules,
          data1:activeData
        })
      } else {
        res.json({
          status: 400,
          message: 'Not Data Found'
        })
      }
    }else{
      res.json({
        status: 400,
        message: 'tenant_id are required....'
      })
    }

  } catch (error) {
    // console.log(`Error: ${error.message}`)
    return res.status(500).json({
      error: err.message
    })
  }
}



/*
    This route used for get recommendation priority rules using zone + weight || zone || weight value.
    Zone are get by using pickupPincode and deliveryPincode, and its find out using ilogix_standard_pincodes_master, ilogix_standard_other_pincodes and then use consignor_standard_billing_zones table.
*/
exports.getRecommendationPriority = async (req, res, next) => {
  // console.log("req.body---",req.body)
    if (req.body.pickupPincode && req.body.deliveryPincode && req.body.platform && (req.body.pickupPincode !== undefined || req.body.pickupPincode !== 'undefined' || req.body.pickupPincode !== '') && (req.body.deliveryPincode !== undefined || req.body.deliveryPincode !== 'undefined' || req.body.deliveryPincode !== '') && (req.body.platform !== undefined || req.body.platform !== 'undefined' || req.body.platform !== '')) {
        var pickupPincode = trim(req.body.pickupPincode)
        var deliveryPincode = trim(req.body.deliveryPincode)
        var sql = "select pm.*,pm.state as state_id,pm.city as city_id from ilogix_standard_pincodes_master pm where pm.pincode IN (" + pickupPincode + ',' + deliveryPincode + ")";
        let zone = '';
        dbController.getMysqlDBMaster().then(async function (connection2) {
            connection2.query(sql, (error, results) => {
                if (error) {
                    return res.status(400).json({
                        message: 'Bad Request',
                    })
                } else {
                    //Intra City IC
                    console.log("results---",results)
                    if (results != '' && results.length >= 2) {
                      console.log("results---",results)
                        var pickupCity = results.find(x => x.pincode === pickupPincode).city_id;
                        //  console.log("pickupCity----",pickupCity)
                        var deliveryCity = results.find(x => x.pincode === deliveryPincode).city_id;
                        // console.log("deliveryCity---",deliveryCity)
                        var pickupState = results.find(x => x.pincode === pickupPincode).state_id;
                        //  console.log("pickupState----",pickupState)
                        var deliveryState = results.find(x => x.pincode === deliveryPincode).state_id;
                        // console.log("deliveryState---",deliveryState)
                        if (pickupCity && deliveryCity && pickupCity.toLowerCase() === deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
                            zone = 'IC';
                            var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                            connection2.query(sql9, (error, results) => {
                                if (error) {
                                    return res.status(400).json({
                                        message: 'Bad Request', 
                                        error: error.message
                                    })
                                } else {
                                    if (results != '') {
                                        zone = results[0].zone;
                                        var courier_code_list = ''
                                        commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                            if (priorityRules != 'All fields are required....') {
                                                console.log("priorityRules---", priorityRules)
                                                priorityRules.forEach(function (element) {
                                                    console.log(element.list)
                                                    element.list.forEach(function (code) {
                                                        console.log("courier_code---", code.courier_code)
                                                        if (courier_code_list == '')
                                                            courier_code_list = "'" + code.courier_code + "'"
                                                        else
                                                            courier_code_list += ",'" + code.courier_code + "'"
  
                                                    })
  
                                                })
                                                var tenant_id = trim(req.body.tenant_id)
                                                var platform = trim(req.body.platform)
                                                if(platform.toUpperCase() == 'V1'){
                                                    var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                    dbController.getMysqlDBMaster().then(async function (connection2) {
                                                        connection2.query(sqldb, (error, results4) => {
                                                            if (error) {
                                                                return res.status(400).json({
                                                                    message: 'Bad Request',
                                                                    error: error.message
                                                                })
                                                            } else {
                                                                console.log("results4---", results4)
                                                                results4.forEach(function (eleme) {
                                                                    var database = eleme.uuid
                                                                    console.log("database@@@@---",database)
                                                                    console.log("courier_code_list---", courier_code_list)
                                                                    var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                    // console.log("sql21---", sql21)
                                                                    // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                    // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                    //     var database = trim(req.body.database)
      
                                                                    dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                        connection2.query(sql21, (error, results1) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                    error: error.message
                                                                                })
                                                                            } else {
                                                                                console.log("resultsCouriers1----", results1)
                                                                                // if (results1) {
                                                                                var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
      
                                                                                connection2.query(sql22, (error, results2) => {
                                                                                    if (error) {
                                                                                        return res.status(400).json({
                                                                                            message: 'Bad Request',
                                                                                            error: error.message
                                                                                        })
                                                                                    } else {
                                                                                        console.log("resultsCouriers2----", results2)
                                                                                        var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                            connection2.query(sql23, (error, results3) => {
                                                                                                if (error) {
                                                                                                    return res.status(400).json({
                                                                                                        message: 'Bad Request',
                                                                                                        error: error.message
                                                                                                    })
                                                                                                } else {
      
      
      
                                                                                                }
                                                                                                console.log("resultsCouriers@1----", results1)
                                                                                                console.log("resultsCouriers@2----", results2)
                                                                                                console.log("mastersqldata@3--", results3)
                                                                                                results1.forEach(function (data1) {
                                                                                                    // results3.forEach(function(data){
                                                                                                    const isFound1 = results3.some(data => {
                                                                                                        console.log("results2.courier_code--", data.courier_id)
                                                                                                        console.log("results3.courier_code--", data1.courier_id)
      
                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                            return true;
                                                                                                        }
                                                                                                    });
                                                                                                    console.log("isFound2---", isFound1)
                                                                                                    if (isFound1 === false) {
      
                                                                                                        // results1.forEach(function(element){
                                                                                                        console.log("myid---", data1.courier_id)
                                                                                                        results3.push({
                                                                                                            courier_id: data1.courier_id
                                                                                                        })
                                                                                                        // })
                                                                                                    }
      
                                                                                                    // })
                                                                                                })
      
                                                                                                results2.forEach(function (data1) {
                                                                                                    // results3.forEach(function(data){
                                                                                                    const isFound = results3.some(data => {
                                                                                                        // console.log("mysql.courier_code--",element.courier_code)
                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                            return true;
                                                                                                        }
                                                                                                    });
                                                                                                    console.log("isFound---", isFound)
                                                                                                    if (isFound === true) {
                                                                                                        let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                        if (el)
                                                                                                            results3.splice(results3.indexOf(el), 1);
                                                                                                    }
      
                                                                                                    // })
                                                                                                })
                                                                                                console.log("results3---", results3)
      
                                                                                                commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                    res.json({
                                                                                                        status: 200,
                                                                                                        data: newdata2
                                                                                                    })
                                                                                                })
      
      
                                                                                            })
                                                                                        })
                                                                                    }
                                                                                })
      
                                                                                // }
      
                                                                            }
      
                                                                        })
      
      
                                                                    })
                                                                    // }else{
                                                                    //     res.json({
                                                                    //         status: 400,
                                                                    //         message: 'Validation: database is required...'
                                                                    //       })
                                                                    // }
      
      
      
                                                                    // res.json({
                                                                    //   status: 200,
                                                                    //   data: priorityRules
                                                                    // })
                                                                })
      
      
      
                                                            }
                                                        })
                                                    })
                                                }else if(platform.toUpperCase() == 'V2'){
                                                  console.log("reached on V2")
                                                  var results3 = []
                                                  websites2Model.find({
                                                    _id: tenant_id
                                                  }).then(function(websites){
                                                    // console.log("websites---",websites)
                                                    if(websites!=''){
                                                      websites.forEach(function(data){
                                                          data.enabled_couriers.forEach(function(element){
                                                            results3.push({
                                                              courier_id: element
                                                            })
                                                          })
                                                      })
                                                      console.log("results3--",results3)
                                                      commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                          res.json({
                                                              status: 200,
                                                              data: newdata2
                                                          })
                                                      })
                                                    }else{
                                                      results3 = []
                                                      console.log("results3--",results3)
                                                      commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                          res.json({
                                                              status: 200,
                                                              data: newdata2
                                                          })
                                                      })
                                                    }
                                                  })
                                                 

                                              }
                                               
  
                                            } else {
                                                res.json({
                                                    status: 400,
                                                    data: 'All fields are required....'
                                                })
                                            }
  
                                        })
                                    }
  
                                }
                            })
                        } else if (pickupCity && deliveryCity && pickupCity.toLowerCase() != deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
                            zone = 'IS';
                            var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                            connection2.query(sql9, (error, results) => {
                                if (error) {
                                    return res.status(400).json({
                                        message: 'Bad Request',
                                        error: error.message
                                    })
                                } else {
                                    if (results != '') {
                                        zone = results[0].zone;
                                        var courier_code_list = ''
                                        commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                            if (priorityRules != 'All fields are required....') {
                                                console.log("priorityRules---", priorityRules)
                                                priorityRules.forEach(function (element) {
                                                    console.log(element.list)
                                                    element.list.forEach(function (code) {
                                                        console.log("courier_code---", code.courier_code)
                                                        if (courier_code_list == '')
                                                            courier_code_list = "'" + code.courier_code + "'"
                                                        else
                                                            courier_code_list += ",'" + code.courier_code + "'"
  
                                                    })
  
                                                })
                                                var tenant_id = trim(req.body.tenant_id)
                                                var platform = trim(req.body.platform)
                                                if(platform.toUpperCase() == 'V1'){
                                                    var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                    dbController.getMysqlDBMaster().then(async function (connection2) {
                                                        connection2.query(sqldb, (error, results4) => {
                                                            if (error) {
                                                                return res.status(400).json({
                                                                    message: 'Bad Request',
                                                                    error: error.message
                                                                })
                                                            } else {
                                                                console.log("results4---", results4)
                                                                results4.forEach(function (eleme) {
                                                                    var database = eleme.uuid
                                                                    console.log("database@@@@---",database)
                                                                    console.log("courier_code_list---", courier_code_list)
                                                                    var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                    // console.log("sql21---", sql21)
                                                                    // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                    // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                    //     var database = trim(req.body.database)
      
                                                                    dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                        connection2.query(sql21, (error, results1) => {
                                                                            if (error) {
                                                                                return res.status(400).json({
                                                                                    message: 'Bad Request',
                                                                                    error: error.message
                                                                                })
                                                                            } else {
                                                                                console.log("resultsCouriers1----", results1)
                                                                                // if (results1) {
                                                                                var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
      
                                                                                connection2.query(sql22, (error, results2) => {
                                                                                    if (error) {
                                                                                        return res.status(400).json({
                                                                                            message: 'Bad Request',
                                                                                            error: error.message
                                                                                        })
                                                                                    } else {
                                                                                        console.log("resultsCouriers2----", results2)
                                                                                        var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                            connection2.query(sql23, (error, results3) => {
                                                                                                if (error) {
                                                                                                    return res.status(400).json({
                                                                                                        message: 'Bad Request',
                                                                                                        error: error.message
                                                                                                    })
                                                                                                } else {
      
      
      
                                                                                                }
                                                                                                console.log("resultsCouriers@1----", results1)
                                                                                                console.log("resultsCouriers@2----", results2)
                                                                                                console.log("mastersqldata@3--", results3)
                                                                                                results1.forEach(function (data1) {
                                                                                                    // results3.forEach(function(data){
                                                                                                    const isFound1 = results3.some(data => {
                                                                                                        console.log("results2.courier_code--", data.courier_id)
                                                                                                        console.log("results3.courier_code--", data1.courier_id)
      
                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                            return true;
                                                                                                        }
                                                                                                    });
                                                                                                    console.log("isFound2---", isFound1)
                                                                                                    if (isFound1 === false) {
      
                                                                                                        // results1.forEach(function(element){
                                                                                                        console.log("myid---", data1.courier_id)
                                                                                                        results3.push({
                                                                                                            courier_id: data1.courier_id
                                                                                                        })
                                                                                                        // })
                                                                                                    }
      
                                                                                                    // })
                                                                                                })
      
                                                                                                results2.forEach(function (data1) {
                                                                                                    // results3.forEach(function(data){
                                                                                                    const isFound = results3.some(data => {
                                                                                                        // console.log("mysql.courier_code--",element.courier_code)
                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                            return true;
                                                                                                        }
                                                                                                    });
                                                                                                    console.log("isFound---", isFound)
                                                                                                    if (isFound === true) {
                                                                                                        let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                        if (el)
                                                                                                            results3.splice(results3.indexOf(el), 1);
                                                                                                    }
      
                                                                                                    // })
                                                                                                })
                                                                                                console.log("results3---", results3)
      
                                                                                                commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                    res.json({
                                                                                                        status: 200,
                                                                                                        data: newdata2
                                                                                                    })
                                                                                                })
      
      
                                                                                            })
                                                                                        })
                                                                                    }
                                                                                })
      
                                                                                // }
      
                                                                            }
      
                                                                        })
      
      
                                                                    })
                                                                    // }else{
                                                                    //     res.json({
                                                                    //         status: 400,
                                                                    //         message: 'Validation: database is required...'
                                                                    //       })
                                                                    // }
      
      
      
                                                                    // res.json({
                                                                    //   status: 200,
                                                                    //   data: priorityRules
                                                                    // })
                                                                })
      
      
      
                                                            }
                                                        })
                                                    })
                                                }else if(platform.toUpperCase() == 'V2'){
                                                  console.log("reached on V2")
                                                  var results3 = []
                                                  websites2Model.find({
                                                    _id: tenant_id
                                                  }).then(function(websites){
                                                    // console.log("websites---",websites)
                                                    if(websites!=''){
                                                      websites.forEach(function(data){
                                                          data.enabled_couriers.forEach(function(element){
                                                            results3.push({
                                                              courier_id: element
                                                            })
                                                          })
                                                      })
                                                      console.log("results3--",results3)
                                                      commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                          res.json({
                                                              status: 200,
                                                              data: newdata2
                                                          })
                                                      })
                                                    }else{
                                                      results3 = []
                                                      console.log("results3--",results3)
                                                      commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                          res.json({
                                                              status: 200,
                                                              data: newdata2
                                                          })
                                                      })
                                                    }
                                                  })
                                                 

                                              }
                                               
  
                                            } else {
                                                res.json({
                                                    status: 400,
                                                    data: 'All fields are required....'
                                                })
                                            }
  
                                        })
                                    }
                                }
                            })
                        }
                    } else if (results == '') {
                        zone == '';
                    }
  
                }
                // console.log("zone---",zone)
                if (zone == '') {
                    var zone_main = ['IC', 'IS']
                    const found = zone_main.some(r => zone.includes(r))
                    if (zone == '' || found === false) {
                        //NJ
                        // Custom Delivery List
                        var sql4 = "select * from consignor_standard_billing_zones_alias tz where tz.is_default=0 and tz.is_custom=1 and tz.is_delivery=0 and tz.is_deleted=0";
                        // check for is_delivery
                        connection2.query(sql4, (error, results) => {
                            if (error) {
                                return res.status(400).json({
                                    message: 'Bad Request',
                                })
                            } else {
                                // console.log("results----",results)
                                for (let czone of results) {
                                    zone_code1 = czone.zone_code;
                                }
                                var sql5 = "select * from ilogix_standard_other_pincodes tp where tp.zone_code= '" + zone_code1 + "' AND tp.is_deleted= 0 AND tp.pincode IN (" + deliveryPincode + ") limit 1";
                                connection2.query(sql5, (error, results) => {
                                    if (error) {
                                        return res.status(400).json({
                                            message: 'Bad Request',
                                            error: error.message
                                        })
                                    } else {
                                        // console.log("results@@---",results)
                                        if (results == '') {
                                            var pickupZone4 = [{ zone_code: 'WZ' }]
                                            for (let czone of pickupZone4) {
                                                zone_code5 = czone.zone_code
                                                var sql10 = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code5 + "' AND tp.is_deleted=0 AND tp.pincode IN (" + pickupPincode + "," + deliveryPincode + ") limit 10";
                                                connection2.query(sql10, (error, results) => {
                                                    if (error) {
                                                        return res.status(400).json({
                                                            message: 'Bad Request',
                                                            error: error.message
                                                        })
                                                    } else {
                                                        // console.log("resultsWZ----",results.length)
                                                        if (results.length < 2 || results == '') {
                                                            //EAST
                                                            var pickupZone5 = [{ zone_code: 'EZ' }]
                                                            for (let czone of pickupZone5) {
                                                                zone_code6 = czone.zone_code
                                                                var sql11 = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code6 + "' AND tp.is_deleted=0 AND tp.pincode IN (" + pickupPincode + "," + deliveryPincode + ") limit 10";
                                                                connection2.query(sql11, (error, results) => {
                                                                    if (error) {
                                                                        return res.status(400).json({
                                                                            message: 'Bad Request',
                                                                            error: error.message
                                                                        })
                                                                    } else {
                                                                        // console.log("resultsEZ----",results)
                                                                        if (results.length < 2 || results == '') {
  
                                                                            //NE...North
                                                                            var pickupZone2 = [{ zone_code: 'NZ' }];
                                                                            for (let czone of pickupZone2) {
                                                                                zone_code3 = czone.zone_code
                                                                                // console.log("zone_codeNE----",zone_code3)
                                                                                var sql7 = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code3 + "' AND tp.is_deleted=0 AND tp.pincode IN (" + pickupPincode + "," + deliveryPincode + ") limit 10";
  
                                                                                connection2.query(sql7, (error, results) => {
                                                                                    if (error) {
                                                                                        return res.status(400).json({
                                                                                            message: 'Bad Request',
                                                                                            error: error.message
                                                                                        })
                                                                                    } else {
                                                                                        if (results.length < 2 || results == '') {
                                                                                            //South
                                                                                            var pickupZone6 = [{ zone_code: 'SZ' }]
                                                                                            for (let czone of pickupZone6) {
                                                                                                zone_code7 = czone.zone_code
                                                                                                var sql13 = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code6 + "' AND tp.is_deleted=0 AND tp.pincode IN (" + pickupPincode + "," + deliveryPincode + ") limit 10";
                                                                                                connection2.query(sql13, (error, results) => {
                                                                                                    if (error) {
                                                                                                        return res.status(400).json({
                                                                                                            message: 'Bad Request',
                                                                                                            error: error.message
                                                                                                        })
                                                                                                    } else {
                                                                                                        if (results.length < 2 || results == '') {
                                                                                                            //MM
                                                                                                            var pickupZone1 = [{ zone_code: 'MM' }]
                                                                                                            for (let czone of pickupZone1) {
                                                                                                                zone_code2 = czone.zone_code
                                                                                                                var sql6 = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code2 + "' AND tp.is_deleted=0 AND tp.pincode IN (" + pickupPincode + "," + deliveryPincode + ") limit 2";
  
                                                                                                                connection2.query(sql6, (error, results) => {
                                                                                                                    if (error) {
                                                                                                                        return res.status(400).json({
                                                                                                                            message: 'Bad Request',
                                                                                                                            error: error.message
                                                                                                                        })
                                                                                                                    } else {
                                                                                                                        if (results.length < 2 || results == '') {
                                                                                                                            //ROI
                                                                                                                            var pickupZone3 = [{ zone_code: 'ROI' }];
                                                                                                                            for (let czone of pickupZone3) {
                                                                                                                                zone_code4 = czone.zone_code
                                                                                                                                // console.log("zone_codeROI---", zone_code4)
                                                                                                                                var sql8 = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code4 + "' AND tp.is_deleted=0 AND tp.pincode IN (" + deliveryPincode + ") limit 2";
                                                                                                                                connection2.query(sql8, (error, results) => {
                                                                                                                                    if (error) {
                                                                                                                                        return res.status(400).json({
                                                                                                                                            message: 'Bad Request',
                                                                                                                                            error: error.message
                                                                                                                                        })
                                                                                                                                    } else {
                                                                                                                                        if (results == '') {
                                                                                                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                                                // console.log("priorityRules---", priorityRules)
                                                                                                                                                res.json({
                                                                                                                                                    status: 400,
                                                                                                                                                    data: priorityRules
                                                                                                                                                })
                                                                                                                                            })
                                                                                                                                        } else if (results != '') {
                                                                                                                                            var zpincodes3 = [];
                                                                                                                                            for (let cpincode of results) {
                                                                                                                                                zpincodes3.push(cpincode.pincode)
                                                                                                                                            }
                                                                                                                                            const found5 = zpincodes3.some(r => deliveryPincode.includes(r))
                                                                                                                                            if (found5 !== false) {
                                                                                                                                                zone = zone_code4
                                                                                                                                                var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                                                                                                                                                connection2.query(sql9, (error, results) => {
                                                                                                                                                    if (error) {
                                                                                                                                                        return res.status(400).json({
                                                                                                                                                            message: 'Bad Request',
                                                                                                                                                            error: error.message
                                                                                                                                                        })
                                                                                                                                                    } else {
                                                                                                                                                        if (results != '') {
                                                                                                                                                            zone = results[0].zone;
                                                                                                                                                            var courier_code_list = ''
                                                                                                                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                                                                if (priorityRules != 'All fields are required....') {
                                                                                                                                                                    console.log("priorityRules---", priorityRules)
                                                                                                                                                                    priorityRules.forEach(function (element) {
                                                                                                                                                                        console.log(element.list)
                                                                                                                                                                        element.list.forEach(function (code) {
                                                                                                                                                                            console.log("courier_code---", code.courier_code)
                                                                                                                                                                            if (courier_code_list == '')
                                                                                                                                                                                courier_code_list = "'" + code.courier_code + "'"
                                                                                                                                                                            else
                                                                                                                                                                                courier_code_list += ",'" + code.courier_code + "'"
  
                                                                                                                                                                        })
  
                                                                                                                                                                    })
                                                                                                                                                                    var tenant_id = trim(req.body.tenant_id)
                                                                                                                                                                    var platform = trim(req.body.platform)
                                                                                                                                                                    if(platform.toUpperCase() == 'V1'){
                                                                                                                                                                        var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                                                                                                                                    dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                                                        connection2.query(sqldb, (error, results4) => {
                                                                                                                                                                            if (error) {
                                                                                                                                                                                return res.status(400).json({
                                                                                                                                                                                    message: 'Bad Request',
                                                                                                                                                                                    error: error.message
                                                                                                                                                                                })
                                                                                                                                                                            } else {
                                                                                                                                                                                console.log("results4---", results4)
                                                                                                                                                                                results4.forEach(function (eleme) {
                                                                                                                                                                                    var database = eleme.uuid
                                                                                                                                                                                    console.log("database@@@@---",database)
                                                                                                                                                                                    console.log("courier_code_list---", courier_code_list)
                                                                                                                                                                                    var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                                                                                                                                    // console.log("sql21---", sql21)
                                                                                                                                                                                    // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                                                                                                                                    // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                                                                                                                                    //     var database = trim(req.body.database)
  
                                                                                                                                                                                    dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                                                                                                                                        connection2.query(sql21, (error, results1) => {
                                                                                                                                                                                            if (error) {
                                                                                                                                                                                                return res.status(400).json({
                                                                                                                                                                                                    message: 'Bad Request',
                                                                                                                                                                                                    error: error.message
                                                                                                                                                                                                })
                                                                                                                                                                                            } else {
                                                                                                                                                                                                console.log("resultsCouriers1----", results1)
                                                                                                                                                                                                // if (results1) {
                                                                                                                                                                                                var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
  
                                                                                                                                                                                                connection2.query(sql22, (error, results2) => {
                                                                                                                                                                                                    if (error) {
                                                                                                                                                                                                        return res.status(400).json({
                                                                                                                                                                                                            message: 'Bad Request',
                                                                                                                                                                                                            error: error.message
                                                                                                                                                                                                        })
                                                                                                                                                                                                    } else {
                                                                                                                                                                                                        console.log("resultsCouriers2----", results2)
                                                                                                                                                                                                        var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                                                                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                                                                                            connection2.query(sql23, (error, results3) => {
                                                                                                                                                                                                                if (error) {
                                                                                                                                                                                                                    return res.status(400).json({
                                                                                                                                                                                                                        message: 'Bad Request',
                                                                                                                                                                                                                        error: error.message
                                                                                                                                                                                                                    })
                                                                                                                                                                                                                } else {
  
  
  
                                                                                                                                                                                                                }
                                                                                                                                                                                                                console.log("resultsCouriers@1----", results1)
                                                                                                                                                                                                                console.log("resultsCouriers@2----", results2)
                                                                                                                                                                                                                console.log("mastersqldata@3--", results3)
                                                                                                                                                                                                                results1.forEach(function (data1) {
                                                                                                                                                                                                                    // results3.forEach(function(data){
                                                                                                                                                                                                                    const isFound1 = results3.some(data => {
                                                                                                                                                                                                                        console.log("results2.courier_code--", data.courier_id)
                                                                                                                                                                                                                        console.log("results3.courier_code--", data1.courier_id)
  
                                                                                                                                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                                                            return true;
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    });
                                                                                                                                                                                                                    console.log("isFound2---", isFound1)
                                                                                                                                                                                                                    if (isFound1 === false) {
  
                                                                                                                                                                                                                        // results1.forEach(function(element){
                                                                                                                                                                                                                        console.log("myid---", data1.courier_id)
                                                                                                                                                                                                                        results3.push({
                                                                                                                                                                                                                            courier_id: data1.courier_id
                                                                                                                                                                                                                        })
                                                                                                                                                                                                                        // })
                                                                                                                                                                                                                    }
  
                                                                                                                                                                                                                    // })
                                                                                                                                                                                                                })
  
                                                                                                                                                                                                                results2.forEach(function (data1) {
                                                                                                                                                                                                                    // results3.forEach(function(data){
                                                                                                                                                                                                                    const isFound = results3.some(data => {
                                                                                                                                                                                                                        // console.log("mysql.courier_code--",element.courier_code)
                                                                                                                                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                                                            return true;
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    });
                                                                                                                                                                                                                    console.log("isFound---", isFound)
                                                                                                                                                                                                                    if (isFound === true) {
                                                                                                                                                                                                                        let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                                                                                                                                        if (el)
                                                                                                                                                                                                                            results3.splice(results3.indexOf(el), 1);
                                                                                                                                                                                                                    }
  
                                                                                                                                                                                                                    // })
                                                                                                                                                                                                                })
                                                                                                                                                                                                                console.log("results3---", results3)
  
                                                                                                                                                                                                                commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                                                                                    res.json({
                                                                                                                                                                                                                        status: 200,
                                                                                                                                                                                                                        data: newdata2
                                                                                                                                                                                                                    })
                                                                                                                                                                                                                })
  
  
                                                                                                                                                                                                            })
                                                                                                                                                                                                        })
                                                                                                                                                                                                    }
                                                                                                                                                                                                })
  
                                                                                                                                                                                                // }
  
                                                                                                                                                                                            }
  
                                                                                                                                                                                        })
  
  
                                                                                                                                                                                    })
                                                                                                                                                                                    // }else{
                                                                                                                                                                                    //     res.json({
                                                                                                                                                                                    //         status: 400,
                                                                                                                                                                                    //         message: 'Validation: database is required...'
                                                                                                                                                                                    //       })
                                                                                                                                                                                    // }
  
  
  
                                                                                                                                                                                    // res.json({
                                                                                                                                                                                    //   status: 200,
                                                                                                                                                                                    //   data: priorityRules
                                                                                                                                                                                    // })
                                                                                                                                                                                })
  
  
  
                                                                                                                                                                            }
                                                                                                                                                                        })
                                                                                                                                                                    })
                                                                                                                                                                    }else if(platform.toUpperCase() == 'V2'){
                                                                                                                                                                      console.log("reached on V2")
                                                                                                                                                                      var results3 = []
                                                                                                                                                                      websites2Model.find({
                                                                                                                                                                        _id: tenant_id
                                                                                                                                                                      }).then(function(websites){
                                                                                                                                                                        // console.log("websites---",websites)
                                                                                                                                                                        if(websites!=''){
                                                                                                                                                                          websites.forEach(function(data){
                                                                                                                                                                              data.enabled_couriers.forEach(function(element){
                                                                                                                                                                                results3.push({
                                                                                                                                                                                  courier_id: element
                                                                                                                                                                                })
                                                                                                                                                                              })
                                                                                                                                                                          })
                                                                                                                                                                          console.log("results3--",results3)
                                                                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                                              res.json({
                                                                                                                                                                                  status: 200,
                                                                                                                                                                                  data: newdata2
                                                                                                                                                                              })
                                                                                                                                                                          })
                                                                                                                                                                        }else{
                                                                                                                                                                          results3 = []
                                                                                                                                                                          console.log("results3--",results3)
                                                                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                                              res.json({
                                                                                                                                                                                  status: 200,
                                                                                                                                                                                  data: newdata2
                                                                                                                                                                              })
                                                                                                                                                                          })
                                                                                                                                                                        }
                                                                                                                                                                      })
                                                                                                                                                                     
                                                                                                                    
                                                                                                                                                                  }
                                                                                                                                                                    
                                                                                                                                                          
                                                                                                                                                                } else {
                                                                                                                                                                    res.json({
                                                                                                                                                                        status: 400,
                                                                                                                                                                        data: 'All fields are required....'
                                                                                                                                                                    })
                                                                                                                                                                }
  
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                })
                                                                                                                                            } else {
                                                                                                                                                commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                                                    // console.log("priorityRules---", priorityRules)
                                                                                                                                                    res.json({
                                                                                                                                                        status: 400,
                                                                                                                                                        data: priorityRules
                                                                                                                                                    })
                                                                                                                                                })
                                                                                                                                            }
                                                                                                                                        }
  
                                                                                                                                    }
                                                                                                                                })
                                                                                                                            }
                                                                                                                        } else if (results != '') {
                                                                                                                            var zpincodes1 = [];
                                                                                                                            for (let cpincode of results) {
                                                                                                                                zpincodes1.push(cpincode.pincode)
                                                                                                                            }
  
  
                                                                                                                            const found3 = zpincodes1.some(r => pickupPincode.includes(r))
                                                                                                                            const found4 = zpincodes1.some(r => deliveryPincode.includes(r))
                                                                                                                            if (found3 !== false && found4 !== false) {
                                                                                                                                zone = zone_code2
                                                                                                                                var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                                                                                                                                connection2.query(sql9, (error, results) => {
                                                                                                                                    if (error) {
                                                                                                                                        return res.status(400).json({
                                                                                                                                            message: 'Bad Request',
                                                                                                                                            error: error.message
                                                                                                                                        })
                                                                                                                                    } else {
                                                                                                                                        if (results != '') {
                                                                                                                                            zone = results[0].zone;
                                                                                                                                            var courier_code_list = ''
                                                                                                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                                                if (priorityRules != 'All fields are required....') {
                                                                                                                                                    console.log("priorityRules---", priorityRules)
                                                                                                                                                    priorityRules.forEach(function (element) {
                                                                                                                                                        console.log(element.list)
                                                                                                                                                        element.list.forEach(function (code) {
                                                                                                                                                            console.log("courier_code---", code.courier_code)
                                                                                                                                                            if (courier_code_list == '')
                                                                                                                                                                courier_code_list = "'" + code.courier_code + "'"
                                                                                                                                                            else
                                                                                                                                                                courier_code_list += ",'" + code.courier_code + "'"
  
                                                                                                                                                        })
  
                                                                                                                                                    })
                                                                                                                                                    var tenant_id = trim(req.body.tenant_id)
                                                                                                                                                    var platform = trim(req.body.platform)
                                                                                                                                                    if(platform.toUpperCase() == 'V1'){
                                                                                                                                                        var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                                                                                                                    dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                                        connection2.query(sqldb, (error, results4) => {
                                                                                                                                                            if (error) {
                                                                                                                                                                return res.status(400).json({
                                                                                                                                                                    message: 'Bad Request',
                                                                                                                                                                    error: error.message
                                                                                                                                                                })
                                                                                                                                                            } else {
                                                                                                                                                                console.log("results4---", results4)
                                                                                                                                                                results4.forEach(function (eleme) {
                                                                                                                                                                    var database = eleme.uuid
                                                                                                                                                                    console.log("database@@@@---",database)
                                                                                                                                                                    console.log("courier_code_list---", courier_code_list)
                                                                                                                                                                    var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                                                                                                                    // console.log("sql21---", sql21)
                                                                                                                                                                    // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                                                                                                                    // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                                                                                                                    //     var database = trim(req.body.database)
  
                                                                                                                                                                    dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                                                                                                                        connection2.query(sql21, (error, results1) => {
                                                                                                                                                                            if (error) {
                                                                                                                                                                                return res.status(400).json({
                                                                                                                                                                                    message: 'Bad Request',
                                                                                                                                                                                    error: error.message
                                                                                                                                                                                })
                                                                                                                                                                            } else {
                                                                                                                                                                                console.log("resultsCouriers1----", results1)
                                                                                                                                                                                // if (results1) {
                                                                                                                                                                                var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
  
                                                                                                                                                                                connection2.query(sql22, (error, results2) => {
                                                                                                                                                                                    if (error) {
                                                                                                                                                                                        return res.status(400).json({
                                                                                                                                                                                            message: 'Bad Request',
                                                                                                                                                                                            error: error.message
                                                                                                                                                                                        })
                                                                                                                                                                                    } else {
                                                                                                                                                                                        console.log("resultsCouriers2----", results2)
                                                                                                                                                                                        var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                                                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                                                                            connection2.query(sql23, (error, results3) => {
                                                                                                                                                                                                if (error) {
                                                                                                                                                                                                    return res.status(400).json({
                                                                                                                                                                                                        message: 'Bad Request',
                                                                                                                                                                                                        error: error.message
                                                                                                                                                                                                    })
                                                                                                                                                                                                } else {
  
  
  
                                                                                                                                                                                                }
                                                                                                                                                                                                console.log("resultsCouriers@1----", results1)
                                                                                                                                                                                                console.log("resultsCouriers@2----", results2)
                                                                                                                                                                                                console.log("mastersqldata@3--", results3)
                                                                                                                                                                                                results1.forEach(function (data1) {
                                                                                                                                                                                                    // results3.forEach(function(data){
                                                                                                                                                                                                    const isFound1 = results3.some(data => {
                                                                                                                                                                                                        console.log("results2.courier_code--", data.courier_id)
                                                                                                                                                                                                        console.log("results3.courier_code--", data1.courier_id)
  
                                                                                                                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                                            return true;
                                                                                                                                                                                                        }
                                                                                                                                                                                                    });
                                                                                                                                                                                                    console.log("isFound2---", isFound1)
                                                                                                                                                                                                    if (isFound1 === false) {
  
                                                                                                                                                                                                        // results1.forEach(function(element){
                                                                                                                                                                                                        console.log("myid---", data1.courier_id)
                                                                                                                                                                                                        results3.push({
                                                                                                                                                                                                            courier_id: data1.courier_id
                                                                                                                                                                                                        })
                                                                                                                                                                                                        // })
                                                                                                                                                                                                    }
  
                                                                                                                                                                                                    // })
                                                                                                                                                                                                })
  
                                                                                                                                                                                                results2.forEach(function (data1) {
                                                                                                                                                                                                    // results3.forEach(function(data){
                                                                                                                                                                                                    const isFound = results3.some(data => {
                                                                                                                                                                                                        // console.log("mysql.courier_code--",element.courier_code)
                                                                                                                                                                                                        if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                                            return true;
                                                                                                                                                                                                        }
                                                                                                                                                                                                    });
                                                                                                                                                                                                    console.log("isFound---", isFound)
                                                                                                                                                                                                    if (isFound === true) {
                                                                                                                                                                                                        let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                                                                                                                        if (el)
                                                                                                                                                                                                            results3.splice(results3.indexOf(el), 1);
                                                                                                                                                                                                    }
  
                                                                                                                                                                                                    // })
                                                                                                                                                                                                })
                                                                                                                                                                                                console.log("results3---", results3)
  
                                                                                                                                                                                                commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                                                                    res.json({
                                                                                                                                                                                                        status: 200,
                                                                                                                                                                                                        data: newdata2
                                                                                                                                                                                                    })
                                                                                                                                                                                                })
  
  
                                                                                                                                                                                            })
                                                                                                                                                                                        })
                                                                                                                                                                                    }
                                                                                                                                                                                })
  
                                                                                                                                                                                // }
  
                                                                                                                                                                            }
  
                                                                                                                                                                        })
  
  
                                                                                                                                                                    })
                                                                                                                                                                    // }else{
                                                                                                                                                                    //     res.json({
                                                                                                                                                                    //         status: 400,
                                                                                                                                                                    //         message: 'Validation: database is required...'
                                                                                                                                                                    //       })
                                                                                                                                                                    // }
  
  
  
                                                                                                                                                                    // res.json({
                                                                                                                                                                    //   status: 200,
                                                                                                                                                                    //   data: priorityRules
                                                                                                                                                                    // })
                                                                                                                                                                })
  
  
  
                                                                                                                                                            }
                                                                                                                                                        })
                                                                                                                                                    })
                                                                                                                                                    }else if(platform.toUpperCase() == 'V2'){
                                                                                                                                                      console.log("reached on V2")
                                                                                                                                                      var results3 = []
                                                                                                                                                      websites2Model.find({
                                                                                                                                                        _id: tenant_id
                                                                                                                                                      }).then(function(websites){
                                                                                                                                                        // console.log("websites---",websites)
                                                                                                                                                        if(websites!=''){
                                                                                                                                                          websites.forEach(function(data){
                                                                                                                                                              data.enabled_couriers.forEach(function(element){
                                                                                                                                                                results3.push({
                                                                                                                                                                  courier_id: element
                                                                                                                                                                })
                                                                                                                                                              })
                                                                                                                                                          })
                                                                                                                                                          console.log("results3--",results3)
                                                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                              res.json({
                                                                                                                                                                  status: 200,
                                                                                                                                                                  data: newdata2
                                                                                                                                                              })
                                                                                                                                                          })
                                                                                                                                                        }else{
                                                                                                                                                          results3 = []
                                                                                                                                                          console.log("results3--",results3)
                                                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                              res.json({
                                                                                                                                                                  status: 200,
                                                                                                                                                                  data: newdata2
                                                                                                                                                              })
                                                                                                                                                          })
                                                                                                                                                        }
                                                                                                                                                      })
                                                                                                                                                     
                                                                                                    
                                                                                                                                                  }
  
                                                                                                                                                    
                                                                                                                                                    
  
                                                                                                                                                } else {
                                                                                                                                                    res.json({
                                                                                                                                                        status: 400,
                                                                                                                                                        data: 'All fields are required....'
                                                                                                                                                    })
                                                                                                                                                }
  
                                                                                                                                            })
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                })
                                                                                                                            } else {
                                                                                                                                commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                                    // console.log("priorityRules---", priorityRules)
                                                                                                                                    res.json({
                                                                                                                                        status: 400,
                                                                                                                                        data: priorityRules
                                                                                                                                    })
                                                                                                                                })
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                })
                                                                                                            }
                                                                                                        } else if (results != '') {
                                                                                                            var zpincodes6 = [];
                                                                                                            for (let cpincode of results) {
                                                                                                                zpincodes6.push(cpincode.pincode)
                                                                                                            }
                                                                                                            const found8 = zpincodes6.some(r => pickupPincode.includes(r))
                                                                                                            const found9 = zpincodes6.some(r => deliveryPincode.includes(r))
                                                                                                            if (found8 !== false && found9 !== false) {
                                                                                                                zone = zone_code7
                                                                                                                var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                                                                                                                connection2.query(sql9, (error, results) => {
                                                                                                                    if (error) {
                                                                                                                        return res.status(400).json({
                                                                                                                            message: 'Bad Request',
                                                                                                                            error: error.message
                                                                                                                        })
                                                                                                                    } else {
                                                                                                                        if (results != '') {
                                                                                                                            zone = results[0].zone;
                                                                                                                            var courier_code_list = ''
                                                                                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                                if (priorityRules != 'All fields are required....') {
                                                                                                                                    console.log("priorityRules---", priorityRules)
                                                                                                                                    priorityRules.forEach(function (element) {
                                                                                                                                        console.log(element.list)
                                                                                                                                        element.list.forEach(function (code) {
                                                                                                                                            console.log("courier_code---", code.courier_code)
                                                                                                                                            if (courier_code_list == '')
                                                                                                                                                courier_code_list = "'" + code.courier_code + "'"
                                                                                                                                            else
                                                                                                                                                courier_code_list += ",'" + code.courier_code + "'"
  
                                                                                                                                        })
  
                                                                                                                                    })
                                                                                                                                    var tenant_id = trim(req.body.tenant_id)
                                                                                                                                    var platform = trim(req.body.platform)
                                                                                                                                    if(platform.toUpperCase() == 'V1'){
                                                                                                                                        var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                            connection2.query(sqldb, (error, results4) => {
                                                                                                                                                if (error) {
                                                                                                                                                    return res.status(400).json({
                                                                                                                                                        message: 'Bad Request',
                                                                                                                                                        error: error.message
                                                                                                                                                    })
                                                                                                                                                } else {
                                                                                                                                                    console.log("results4---", results4)
                                                                                                                                                    results4.forEach(function (eleme) {
                                                                                                                                                        var database = eleme.uuid
                                                                                                                                                        console.log("database@@@@---",database)
                                                                                                                                                        console.log("courier_code_list---", courier_code_list)
                                                                                                                                                        var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                                                                                                        // console.log("sql21---", sql21)
                                                                                                                                                        // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                                                                                                        // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                                                                                                        //     var database = trim(req.body.database)
      
                                                                                                                                                        dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                                                                                                            connection2.query(sql21, (error, results1) => {
                                                                                                                                                                if (error) {
                                                                                                                                                                    return res.status(400).json({
                                                                                                                                                                        message: 'Bad Request',
                                                                                                                                                                        error: error.message
                                                                                                                                                                    })
                                                                                                                                                                } else {
                                                                                                                                                                    console.log("resultsCouriers1----", results1)
                                                                                                                                                                    // if (results1) {
                                                                                                                                                                    var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
      
                                                                                                                                                                    connection2.query(sql22, (error, results2) => {
                                                                                                                                                                        if (error) {
                                                                                                                                                                            return res.status(400).json({
                                                                                                                                                                                message: 'Bad Request',
                                                                                                                                                                                error: error.message
                                                                                                                                                                            })
                                                                                                                                                                        } else {
                                                                                                                                                                            console.log("resultsCouriers2----", results2)
                                                                                                                                                                            var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                                                                                                            dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                                                                connection2.query(sql23, (error, results3) => {
                                                                                                                                                                                    if (error) {
                                                                                                                                                                                        return res.status(400).json({
                                                                                                                                                                                            message: 'Bad Request',
                                                                                                                                                                                            error: error.message
                                                                                                                                                                                        })
                                                                                                                                                                                    } else {
      
      
      
                                                                                                                                                                                    }
                                                                                                                                                                                    console.log("resultsCouriers@1----", results1)
                                                                                                                                                                                    console.log("resultsCouriers@2----", results2)
                                                                                                                                                                                    console.log("mastersqldata@3--", results3)
                                                                                                                                                                                    results1.forEach(function (data1) {
                                                                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                                                                        const isFound1 = results3.some(data => {
                                                                                                                                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                                                                                                                                            console.log("results3.courier_code--", data1.courier_id)
      
                                                                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                                return true;
                                                                                                                                                                                            }
                                                                                                                                                                                        });
                                                                                                                                                                                        console.log("isFound2---", isFound1)
                                                                                                                                                                                        if (isFound1 === false) {
      
                                                                                                                                                                                            // results1.forEach(function(element){
                                                                                                                                                                                            console.log("myid---", data1.courier_id)
                                                                                                                                                                                            results3.push({
                                                                                                                                                                                                courier_id: data1.courier_id
                                                                                                                                                                                            })
                                                                                                                                                                                            // })
                                                                                                                                                                                        }
      
                                                                                                                                                                                        // })
                                                                                                                                                                                    })
      
                                                                                                                                                                                    results2.forEach(function (data1) {
                                                                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                                                                        const isFound = results3.some(data => {
                                                                                                                                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                                return true;
                                                                                                                                                                                            }
                                                                                                                                                                                        });
                                                                                                                                                                                        console.log("isFound---", isFound)
                                                                                                                                                                                        if (isFound === true) {
                                                                                                                                                                                            let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                                                                                                            if (el)
                                                                                                                                                                                                results3.splice(results3.indexOf(el), 1);
                                                                                                                                                                                        }
      
                                                                                                                                                                                        // })
                                                                                                                                                                                    })
                                                                                                                                                                                    console.log("results3---", results3)
      
                                                                                                                                                                                    commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                                                        res.json({
                                                                                                                                                                                            status: 200,
                                                                                                                                                                                            data: newdata2
                                                                                                                                                                                        })
                                                                                                                                                                                    })
      
      
                                                                                                                                                                                })
                                                                                                                                                                            })
                                                                                                                                                                        }
                                                                                                                                                                    })
      
                                                                                                                                                                    // }
      
                                                                                                                                                                }
      
                                                                                                                                                            })
      
      
                                                                                                                                                        })
                                                                                                                                                        // }else{
                                                                                                                                                        //     res.json({
                                                                                                                                                        //         status: 400,
                                                                                                                                                        //         message: 'Validation: database is required...'
                                                                                                                                                        //       })
                                                                                                                                                        // }
      
      
      
                                                                                                                                                        // res.json({
                                                                                                                                                        //   status: 200,
                                                                                                                                                        //   data: priorityRules
                                                                                                                                                        // })
                                                                                                                                                    })
      
      
      
                                                                                                                                                }
                                                                                                                                            })
                                                                                                                                        })
                                                                                                                                    }else if(platform.toUpperCase() == 'V2'){
                                                                                                                                      console.log("reached on V2")
                                                                                                                                      var results3 = []
                                                                                                                                      websites2Model.find({
                                                                                                                                        _id: tenant_id
                                                                                                                                      }).then(function(websites){
                                                                                                                                        // console.log("websites---",websites)
                                                                                                                                        if(websites!=''){
                                                                                                                                          websites.forEach(function(data){
                                                                                                                                              data.enabled_couriers.forEach(function(element){
                                                                                                                                                results3.push({
                                                                                                                                                  courier_id: element
                                                                                                                                                })
                                                                                                                                              })
                                                                                                                                          })
                                                                                                                                          console.log("results3--",results3)
                                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                              res.json({
                                                                                                                                                  status: 200,
                                                                                                                                                  data: newdata2
                                                                                                                                              })
                                                                                                                                          })
                                                                                                                                        }else{
                                                                                                                                          results3 = []
                                                                                                                                          console.log("results3--",results3)
                                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                              res.json({
                                                                                                                                                  status: 200,
                                                                                                                                                  data: newdata2
                                                                                                                                              })
                                                                                                                                          })
                                                                                                                                        }
                                                                                                                                      })
                                                                                                                                     
                                                                                    
                                                                                                                                  }
          
  
                                                                                                                                } else {
                                                                                                                                    res.json({
                                                                                                                                        status: 400,
                                                                                                                                        data: 'All fields are required....'
                                                                                                                                    })
                                                                                                                                }
  
                                                                                                                            })
                                                                                                                        }
                                                                                                                    }
                                                                                                                })
                                                                                                            } else {
                                                                                                                commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                    // console.log("priorityRules---", priorityRules)
                                                                                                                    res.json({
                                                                                                                        status: 400,
                                                                                                                        data: priorityRules
                                                                                                                    })
                                                                                                                })
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                        } else {
                                                                                            var zpincodes2 = [];
                                                                                            for (let cpincode of results) {
                                                                                                zpincodes2.push(cpincode.pincode)
                                                                                            }
                                                                                            const found5 = zpincodes2.some(r => deliveryPincode.includes(r))
                                                                                            const found6 = zpincodes2.some(r => deliveryPincode.includes(r))
  
                                                                                            if (found5 !== false && found6 !== false) {
                                                                                                zone = zone_code3
                                                                                                var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                                                                                                connection2.query(sql9, (error, results) => {
                                                                                                    if (error) {
                                                                                                        return res.status(400).json({
                                                                                                            message: 'Bad Request',
                                                                                                            error: error.message
                                                                                                        })
                                                                                                    } else {
                                                                                                        if (results != '') {
                                                                                                            zone = results[0].zone;
                                                                                                            var courier_code_list = ''
                                                                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                                if (priorityRules != 'All fields are required....') {
                                                                                                                    console.log("priorityRules---", priorityRules)
                                                                                                                    priorityRules.forEach(function (element) {
                                                                                                                        console.log(element.list)
                                                                                                                        element.list.forEach(function (code) {
                                                                                                                            console.log("courier_code---", code.courier_code)
                                                                                                                            if (courier_code_list == '')
                                                                                                                                courier_code_list = "'" + code.courier_code + "'"
                                                                                                                            else
                                                                                                                                courier_code_list += ",'" + code.courier_code + "'"
  
                                                                                                                        })
  
                                                                                                                    })
                                                                                                                    var tenant_id = trim(req.body.tenant_id)
                                                                                                                    var platform = trim(req.body.platform)
                                                                                                                    if(platform.toUpperCase() == 'V1'){
                                                                                                                        var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                            connection2.query(sqldb, (error, results4) => {
                                                                                                                                if (error) {
                                                                                                                                    return res.status(400).json({
                                                                                                                                        message: 'Bad Request',
                                                                                                                                        error: error.message
                                                                                                                                    })
                                                                                                                                } else {
                                                                                                                                    console.log("results4---", results4)
                                                                                                                                    results4.forEach(function (eleme) {
                                                                                                                                        var database = eleme.uuid
                                                                                                                                        console.log("database@@@@---",database)
                                                                                                                                        console.log("courier_code_list---", courier_code_list)
                                                                                                                                        var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                                                                                        // console.log("sql21---", sql21)
                                                                                                                                        // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                                                                                        // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                                                                                        //     var database = trim(req.body.database)
      
                                                                                                                                        dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                                                                                            connection2.query(sql21, (error, results1) => {
                                                                                                                                                if (error) {
                                                                                                                                                    return res.status(400).json({
                                                                                                                                                        message: 'Bad Request',
                                                                                                                                                        error: error.message
                                                                                                                                                    })
                                                                                                                                                } else {
                                                                                                                                                    console.log("resultsCouriers1----", results1)
                                                                                                                                                    // if (results1) {
                                                                                                                                                    var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
      
                                                                                                                                                    connection2.query(sql22, (error, results2) => {
                                                                                                                                                        if (error) {
                                                                                                                                                            return res.status(400).json({
                                                                                                                                                                message: 'Bad Request',
                                                                                                                                                                error: error.message
                                                                                                                                                            })
                                                                                                                                                        } else {
                                                                                                                                                            console.log("resultsCouriers2----", results2)
                                                                                                                                                            var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                                                                                            dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                                                connection2.query(sql23, (error, results3) => {
                                                                                                                                                                    if (error) {
                                                                                                                                                                        return res.status(400).json({
                                                                                                                                                                            message: 'Bad Request',
                                                                                                                                                                            error: error.message
                                                                                                                                                                        })
                                                                                                                                                                    } else {
      
      
      
                                                                                                                                                                    }
                                                                                                                                                                    console.log("resultsCouriers@1----", results1)
                                                                                                                                                                    console.log("resultsCouriers@2----", results2)
                                                                                                                                                                    console.log("mastersqldata@3--", results3)
                                                                                                                                                                    results1.forEach(function (data1) {
                                                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                                                        const isFound1 = results3.some(data => {
                                                                                                                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                                                                                                                            console.log("results3.courier_code--", data1.courier_id)
      
                                                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                return true;
                                                                                                                                                                            }
                                                                                                                                                                        });
                                                                                                                                                                        console.log("isFound2---", isFound1)
                                                                                                                                                                        if (isFound1 === false) {
      
                                                                                                                                                                            // results1.forEach(function(element){
                                                                                                                                                                            console.log("myid---", data1.courier_id)
                                                                                                                                                                            results3.push({
                                                                                                                                                                                courier_id: data1.courier_id
                                                                                                                                                                            })
                                                                                                                                                                            // })
                                                                                                                                                                        }
      
                                                                                                                                                                        // })
                                                                                                                                                                    })
      
                                                                                                                                                                    results2.forEach(function (data1) {
                                                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                                                        const isFound = results3.some(data => {
                                                                                                                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                                                return true;
                                                                                                                                                                            }
                                                                                                                                                                        });
                                                                                                                                                                        console.log("isFound---", isFound)
                                                                                                                                                                        if (isFound === true) {
                                                                                                                                                                            let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                                                                                            if (el)
                                                                                                                                                                                results3.splice(results3.indexOf(el), 1);
                                                                                                                                                                        }
      
                                                                                                                                                                        // })
                                                                                                                                                                    })
                                                                                                                                                                    console.log("results3---", results3)
      
                                                                                                                                                                    commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                                        res.json({
                                                                                                                                                                            status: 200,
                                                                                                                                                                            data: newdata2
                                                                                                                                                                        })
                                                                                                                                                                    })
      
      
                                                                                                                                                                })
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                    })
      
                                                                                                                                                    // }
      
                                                                                                                                                }
      
                                                                                                                                            })
      
      
                                                                                                                                        })
                                                                                                                                        // }else{
                                                                                                                                        //     res.json({
                                                                                                                                        //         status: 400,
                                                                                                                                        //         message: 'Validation: database is required...'
                                                                                                                                        //       })
                                                                                                                                        // }
      
      
      
                                                                                                                                        // res.json({
                                                                                                                                        //   status: 200,
                                                                                                                                        //   data: priorityRules
                                                                                                                                        // })
                                                                                                                                    })
      
      
      
                                                                                                                                }
                                                                                                                            })
                                                                                                                        })
                                                                                                                    }else if(platform.toUpperCase() == 'V2'){
                                                                                                                      console.log("reached on V2")
                                                                                                                      var results3 = []
                                                                                                                      websites2Model.find({
                                                                                                                        _id: tenant_id
                                                                                                                      }).then(function(websites){
                                                                                                                        // console.log("websites---",websites)
                                                                                                                        if(websites!=''){
                                                                                                                          websites.forEach(function(data){
                                                                                                                              data.enabled_couriers.forEach(function(element){
                                                                                                                                results3.push({
                                                                                                                                  courier_id: element
                                                                                                                                })
                                                                                                                              })
                                                                                                                          })
                                                                                                                          console.log("results3--",results3)
                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                              res.json({
                                                                                                                                  status: 200,
                                                                                                                                  data: newdata2
                                                                                                                              })
                                                                                                                          })
                                                                                                                        }else{
                                                                                                                          results3 = []
                                                                                                                          console.log("results3--",results3)
                                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                              res.json({
                                                                                                                                  status: 200,
                                                                                                                                  data: newdata2
                                                                                                                              })
                                                                                                                          })
                                                                                                                        }
                                                                                                                      })
                                                                                                                     
                                                                    
                                                                                                                  }
           
  
                                                                                                                } else {
                                                                                                                    res.json({
                                                                                                                        status: 400,
                                                                                                                        data: 'All fields are required....'
                                                                                                                    })
                                                                                                                }
  
                                                                                                            })
                                                                                                        }
                                                                                                    }
                                                                                                })
                                                                                            } else {
                                                                                                commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                    // console.log("priorityRules---", priorityRules)
                                                                                                    res.json({
                                                                                                        status: 400,
                                                                                                        data: priorityRules
                                                                                                    })
                                                                                                })
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                })
                                                                            }
                                                                        } else if (results != '') {
                                                                            var zpincodes5 = [];
                                                                            for (let cpincode of results) {
                                                                                zpincodes5.push(cpincode.pincode)
                                                                            }
                                                                            const found6 = zpincodes5.some(r => pickupPincode.includes(r))
                                                                            const found7 = zpincodes5.some(r => deliveryPincode.includes(r))
                                                                            if (found6 !== false && found7 !== false) {
                                                                                zone = zone_code6
                                                                                var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                                                                                connection2.query(sql9, (error, results) => {
                                                                                    if (error) {
                                                                                        return res.status(400).json({
                                                                                            message: 'Bad Request',
                                                                                            error: error.message
                                                                                        })
                                                                                    } else {
                                                                                        if (results != '') {
                                                                                            zone = results[0].zone;
                                                                                            var courier_code_list = ''
                                                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                                if (priorityRules != 'All fields are required....') {
                                                                                                    console.log("priorityRules---", priorityRules)
                                                                                                    priorityRules.forEach(function (element) {
                                                                                                        console.log(element.list)
                                                                                                        element.list.forEach(function (code) {
                                                                                                            console.log("courier_code---", code.courier_code)
                                                                                                            if (courier_code_list == '')
                                                                                                                courier_code_list = "'" + code.courier_code + "'"
                                                                                                            else
                                                                                                                courier_code_list += ",'" + code.courier_code + "'"
  
                                                                                                        })
  
                                                                                                    })
                                                                                                    var tenant_id = trim(req.body.tenant_id)
                                                                                                    var platform = trim(req.body.platform)
                                                                                                    if(platform.toUpperCase() == 'V1'){
                                                                                                        var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                            connection2.query(sqldb, (error, results4) => {
                                                                                                                if (error) {
                                                                                                                    return res.status(400).json({
                                                                                                                        message: 'Bad Request',
                                                                                                                        error: error.message
                                                                                                                    })
                                                                                                                } else {
                                                                                                                    console.log("results4---", results4)
                                                                                                                    results4.forEach(function (eleme) {
                                                                                                                        var database = eleme.uuid
                                                                                                                        console.log("database@@@@---",database)
                                                                                                                        console.log("courier_code_list---", courier_code_list)
                                                                                                                        var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                                                                        // console.log("sql21---", sql21)
                                                                                                                        // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                                                                        // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                                                                        //     var database = trim(req.body.database)
      
                                                                                                                        dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                                                                            connection2.query(sql21, (error, results1) => {
                                                                                                                                if (error) {
                                                                                                                                    return res.status(400).json({
                                                                                                                                        message: 'Bad Request',
                                                                                                                                        error: error.message
                                                                                                                                    })
                                                                                                                                } else {
                                                                                                                                    console.log("resultsCouriers1----", results1)
                                                                                                                                    // if (results1) {
                                                                                                                                    var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
      
                                                                                                                                    connection2.query(sql22, (error, results2) => {
                                                                                                                                        if (error) {
                                                                                                                                            return res.status(400).json({
                                                                                                                                                message: 'Bad Request',
                                                                                                                                                error: error.message
                                                                                                                                            })
                                                                                                                                        } else {
                                                                                                                                            console.log("resultsCouriers2----", results2)
                                                                                                                                            var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                                                                            dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                                connection2.query(sql23, (error, results3) => {
                                                                                                                                                    if (error) {
                                                                                                                                                        return res.status(400).json({
                                                                                                                                                            message: 'Bad Request',
                                                                                                                                                            error: error.message
                                                                                                                                                        })
                                                                                                                                                    } else {
      
      
      
                                                                                                                                                    }
                                                                                                                                                    console.log("resultsCouriers@1----", results1)
                                                                                                                                                    console.log("resultsCouriers@2----", results2)
                                                                                                                                                    console.log("mastersqldata@3--", results3)
                                                                                                                                                    results1.forEach(function (data1) {
                                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                                        const isFound1 = results3.some(data => {
                                                                                                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                                                                                                            console.log("results3.courier_code--", data1.courier_id)
      
                                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                                return true;
                                                                                                                                                            }
                                                                                                                                                        });
                                                                                                                                                        console.log("isFound2---", isFound1)
                                                                                                                                                        if (isFound1 === false) {
      
                                                                                                                                                            // results1.forEach(function(element){
                                                                                                                                                            console.log("myid---", data1.courier_id)
                                                                                                                                                            results3.push({
                                                                                                                                                                courier_id: data1.courier_id
                                                                                                                                                            })
                                                                                                                                                            // })
                                                                                                                                                        }
      
                                                                                                                                                        // })
                                                                                                                                                    })
      
                                                                                                                                                    results2.forEach(function (data1) {
                                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                                        const isFound = results3.some(data => {
                                                                                                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                                return true;
                                                                                                                                                            }
                                                                                                                                                        });
                                                                                                                                                        console.log("isFound---", isFound)
                                                                                                                                                        if (isFound === true) {
                                                                                                                                                            let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                                                                            if (el)
                                                                                                                                                                results3.splice(results3.indexOf(el), 1);
                                                                                                                                                        }
      
                                                                                                                                                        // })
                                                                                                                                                    })
                                                                                                                                                    console.log("results3---", results3)
      
                                                                                                                                                    commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                                        res.json({
                                                                                                                                                            status: 200,
                                                                                                                                                            data: newdata2
                                                                                                                                                        })
                                                                                                                                                    })
      
      
                                                                                                                                                })
                                                                                                                                            })
                                                                                                                                        }
                                                                                                                                    })
      
                                                                                                                                    // }
      
                                                                                                                                }
      
                                                                                                                            })
      
      
                                                                                                                        })
                                                                                                                        // }else{
                                                                                                                        //     res.json({
                                                                                                                        //         status: 400,
                                                                                                                        //         message: 'Validation: database is required...'
                                                                                                                        //       })
                                                                                                                        // }
      
      
      
                                                                                                                        // res.json({
                                                                                                                        //   status: 200,
                                                                                                                        //   data: priorityRules
                                                                                                                        // })
                                                                                                                    })
      
      
      
                                                                                                                }
                                                                                                            })
                                                                                                        })
                                                                                                    }else if(platform.toUpperCase() == 'V2'){
                                                                                                      console.log("reached on V2")
                                                                                                      var results3 = []
                                                                                                      websites2Model.find({
                                                                                                        _id: tenant_id
                                                                                                      }).then(function(websites){
                                                                                                        // console.log("websites---",websites)
                                                                                                        if(websites!=''){
                                                                                                          websites.forEach(function(data){
                                                                                                              data.enabled_couriers.forEach(function(element){
                                                                                                                results3.push({
                                                                                                                  courier_id: element
                                                                                                                })
                                                                                                              })
                                                                                                          })
                                                                                                          console.log("results3--",results3)
                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                              res.json({
                                                                                                                  status: 200,
                                                                                                                  data: newdata2
                                                                                                              })
                                                                                                          })
                                                                                                        }else{
                                                                                                          results3 = []
                                                                                                          console.log("results3--",results3)
                                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                              res.json({
                                                                                                                  status: 200,
                                                                                                                  data: newdata2
                                                                                                              })
                                                                                                          })
                                                                                                        }
                                                                                                      })
                                                                                                     
                                                    
                                                                                                  }                                   
                         
  
                                                                                                } else {
                                                                                                    res.json({
                                                                                                        status: 400,
                                                                                                        data: 'All fields are required....'
                                                                                                    })
                                                                                                }
  
                                                                                            })
                                                                                        }
                                                                                    }
                                                                                })
                                                                            } else {
                                                                                commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                    // console.log("priorityRules---", priorityRules)
                                                                                    res.json({
                                                                                        status: 400,
                                                                                        data: priorityRules
                                                                                    })
                                                                                })
                                                                            }
  
                                                                        }
                                                                    }
                                                                })
                                                            }
  
                                                        } else if (results != '') {
                                                            var zpincodes4 = [];
                                                            for (let cpincode of results) {
                                                                zpincodes4.push(cpincode.pincode)
                                                            }
                                                            const found6 = zpincodes4.some(r => pickupPincode.includes(r))
                                                            const found7 = zpincodes4.some(r => deliveryPincode.includes(r))
                                                            if (found6 !== false && found7 !== false) {
                                                                zone = zone_code5
                                                                var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                                                                connection2.query(sql9, (error, results) => {
                                                                    //   console.log("results###---",results)
                                                                    if (error) {
                                                                        return res.status(400).json({
                                                                            message: 'Bad Request',
                                                                            error: error.message
                                                                        })
                                                                    } else {
                                                                        if (results != '') {
                                                                            zone = results[0].zone;
                                                                            var courier_code_list = ''
                                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                                if (priorityRules != 'All fields are required....') {
                                                                                    console.log("priorityRules---", priorityRules)
                                                                                    priorityRules.forEach(function (element) {
                                                                                        console.log(element.list)
                                                                                        element.list.forEach(function (code) {
                                                                                            console.log("courier_code---", code.courier_code)
                                                                                            if (courier_code_list == '')
                                                                                                courier_code_list = "'" + code.courier_code + "'"
                                                                                            else
                                                                                                courier_code_list += ",'" + code.courier_code + "'"
  
                                                                                        })
  
                                                                                    })
                                                                                    var tenant_id = trim(req.body.tenant_id)
                                                                                    var platform = trim(req.body.platform)
                                                                                    if(platform.toUpperCase() == 'V1'){
                                                                                        var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                            connection2.query(sqldb, (error, results4) => {
                                                                                                if (error) {
                                                                                                    return res.status(400).json({
                                                                                                        message: 'Bad Request',
                                                                                                        error: error.message
                                                                                                    })
                                                                                                } else {
                                                                                                    console.log("results4---", results4)
                                                                                                    results4.forEach(function (eleme) {
                                                                                                        var database = eleme.uuid
                                                                                                        console.log("database@@@@---",database)
                                                                                                        console.log("courier_code_list---", courier_code_list)
                                                                                                        var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                                                        // console.log("sql21---", sql21)
                                                                                                        // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                                                        // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                                                        //     var database = trim(req.body.database)
      
                                                                                                        dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                                                            connection2.query(sql21, (error, results1) => {
                                                                                                                if (error) {
                                                                                                                    return res.status(400).json({
                                                                                                                        message: 'Bad Request',
                                                                                                                        error: error.message
                                                                                                                    })
                                                                                                                } else {
                                                                                                                    console.log("resultsCouriers1----", results1)
                                                                                                                    // if (results1) {
                                                                                                                    var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
      
                                                                                                                    connection2.query(sql22, (error, results2) => {
                                                                                                                        if (error) {
                                                                                                                            return res.status(400).json({
                                                                                                                                message: 'Bad Request',
                                                                                                                                error: error.message
                                                                                                                            })
                                                                                                                        } else {
                                                                                                                            console.log("resultsCouriers2----", results2)
                                                                                                                            var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                                                            dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                                connection2.query(sql23, (error, results3) => {
                                                                                                                                    if (error) {
                                                                                                                                        return res.status(400).json({
                                                                                                                                            message: 'Bad Request',
                                                                                                                                            error: error.message
                                                                                                                                        })
                                                                                                                                    } else {
      
      
      
                                                                                                                                    }
                                                                                                                                    console.log("resultsCouriers@1----", results1)
                                                                                                                                    console.log("resultsCouriers@2----", results2)
                                                                                                                                    console.log("mastersqldata@3--", results3)
                                                                                                                                    results1.forEach(function (data1) {
                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                        const isFound1 = results3.some(data => {
                                                                                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                                                                                            console.log("results3.courier_code--", data1.courier_id)
      
                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                return true;
                                                                                                                                            }
                                                                                                                                        });
                                                                                                                                        console.log("isFound2---", isFound1)
                                                                                                                                        if (isFound1 === false) {
      
                                                                                                                                            // results1.forEach(function(element){
                                                                                                                                            console.log("myid---", data1.courier_id)
                                                                                                                                            results3.push({
                                                                                                                                                courier_id: data1.courier_id
                                                                                                                                            })
                                                                                                                                            // })
                                                                                                                                        }
      
                                                                                                                                        // })
                                                                                                                                    })
      
                                                                                                                                    results2.forEach(function (data1) {
                                                                                                                                        // results3.forEach(function(data){
                                                                                                                                        const isFound = results3.some(data => {
                                                                                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                                return true;
                                                                                                                                            }
                                                                                                                                        });
                                                                                                                                        console.log("isFound---", isFound)
                                                                                                                                        if (isFound === true) {
                                                                                                                                            let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                                                            if (el)
                                                                                                                                                results3.splice(results3.indexOf(el), 1);
                                                                                                                                        }
      
                                                                                                                                        // })
                                                                                                                                    })
                                                                                                                                    console.log("results3---", results3)
      
                                                                                                                                    commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                                        res.json({
                                                                                                                                            status: 200,
                                                                                                                                            data: newdata2
                                                                                                                                        })
                                                                                                                                    })
      
      
                                                                                                                                })
                                                                                                                            })
                                                                                                                        }
                                                                                                                    })
      
                                                                                                                    // }
      
                                                                                                                }
      
                                                                                                            })
      
      
                                                                                                        })
                                                                                                        // }else{
                                                                                                        //     res.json({
                                                                                                        //         status: 400,
                                                                                                        //         message: 'Validation: database is required...'
                                                                                                        //       })
                                                                                                        // }
      
      
      
                                                                                                        // res.json({
                                                                                                        //   status: 200,
                                                                                                        //   data: priorityRules
                                                                                                        // })
                                                                                                    })
      
      
      
                                                                                                }
                                                                                            })
                                                                                        })
                                                                                    }else if(platform.toUpperCase() == 'V2'){
                                                                                      console.log("reached on V2")
                                                                                      var results3 = []
                                                                                      websites2Model.find({
                                                                                        _id: tenant_id
                                                                                      }).then(function(websites){
                                                                                        // console.log("websites---",websites)
                                                                                        if(websites!=''){
                                                                                          websites.forEach(function(data){
                                                                                              data.enabled_couriers.forEach(function(element){
                                                                                                results3.push({
                                                                                                  courier_id: element
                                                                                                })
                                                                                              })
                                                                                          })
                                                                                          console.log("results3--",results3)
                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                              res.json({
                                                                                                  status: 200,
                                                                                                  data: newdata2
                                                                                              })
                                                                                          })
                                                                                        }else{
                                                                                          results3 = []
                                                                                          console.log("results3--",results3)
                                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                              res.json({
                                                                                                  status: 200,
                                                                                                  data: newdata2
                                                                                              })
                                                                                          })
                                                                                        }
                                                                                      })
                                                                                     
                                    
                                                                                  }
  
                                                                                    
  
  
                                                                                } else {
                                                                                    res.json({
                                                                                        status: 400,
                                                                                        data: 'All fields are required....'
                                                                                    })
                                                                                }
  
                                                                            })
                                                                        }
                                                                    }
                                                                })
                                                            } else {
                                                                commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                    // console.log("priorityRules---", priorityRules)
                                                                    res.json({
                                                                        status: 400,
                                                                        data: priorityRules
                                                                    })
                                                                })
                                                            }
                                                        }
                                                    }
                                                })
                                            }
                                        } else if (results != '') {
                                            var custom_pickup_pincodes = [];
                                            for (let cpincode of results) {
                                                custom_pickup_pincodes.push(cpincode.pincode)
                                            }
                                            const found1 = custom_pickup_pincodes.some(r => deliveryPincode.includes(r))
                                            if (found1 !== false) {
                                                zone = zone_code1;
                                                var sql9 = "select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone + "'";
                                                connection2.query(sql9, (error, results) => {
                                                    if (error) {
                                                        return res.status(400).json({
                                                            message: 'Bad Request',
                                                            error: error.message
                                                        })
                                                    } else {
                                                        if (results != '') {
                                                            zone = results[0].zone;
                                                            var courier_code_list = ''
                                                            commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                                if (priorityRules != 'All fields are required....') {
                                                                    console.log("priorityRules---", priorityRules)
                                                                    priorityRules.forEach(function (element) {
                                                                        console.log(element.list)
                                                                        element.list.forEach(function (code) {
                                                                            console.log("courier_code---", code.courier_code)
                                                                            if (courier_code_list == '')
                                                                                courier_code_list = "'" + code.courier_code + "'"
                                                                            else
                                                                                courier_code_list += ",'" + code.courier_code + "'"
  
                                                                        })
  
                                                                    })
                                                                    var tenant_id = trim(req.body.tenant_id)
                                                                    var platform = trim(req.body.platform)
                                                                    if(platform.toUpperCase() == 'V1'){
                                                                        var sqldb = 'select uuid from websites where id=' + Number(tenant_id);
                                                                        dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                            connection2.query(sqldb, (error, results4) => {
                                                                                if (error) {
                                                                                    return res.status(400).json({
                                                                                        message: 'Bad Request',
                                                                                        error: error.message
                                                                                    })
                                                                                } else {
                                                                                    console.log("results4---", results4)
                                                                                    results4.forEach(function (eleme) {
                                                                                        var database = eleme.uuid
                                                                                        console.log("database@@@@---",database)
                                                                                        console.log("courier_code_list---", courier_code_list)
                                                                                        var sql21 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
                                                                                        // console.log("sql21---", sql21)
                                                                                        // var database = '17396194dfc544119e24c52c5e9e0dca'
                                                                                        // if (req.body.database && (req.body.database !== undefined || req.body.database !== 'undefined' || req.body.database !== '')) {
                                                                                        //     var database = trim(req.body.database)
      
                                                                                        dbController.getMysqlDBClient(database).then(async function (connection2) {
                                                                                            connection2.query(sql21, (error, results1) => {
                                                                                                if (error) {
                                                                                                    return res.status(400).json({
                                                                                                        message: 'Bad Request',
                                                                                                        error: error.message
                                                                                                    })
                                                                                                } else {
                                                                                                    console.log("resultsCouriers1----", results1)
                                                                                                    // if (results1) {
                                                                                                    var sql22 = 'select DISTINCT cm.courier_code as courier_id from tenant_serviceable_pincodes t left join courier_master cm on t.courier_id=cm.courier_vendor_id where t.pincode="' + deliveryPincode + '" and t.lm_status="-1" and cm.courier_code in (' + courier_code_list + ') order by t.courier_id ASC';
      
                                                                                                    connection2.query(sql22, (error, results2) => {
                                                                                                        if (error) {
                                                                                                            return res.status(400).json({
                                                                                                                message: 'Bad Request',
                                                                                                                error: error.message
                                                                                                            })
                                                                                                        } else {
                                                                                                            console.log("resultsCouriers2----", results2)
                                                                                                            var sql23 = 'select DISTINCT c.code as courier_id from serviceable_pincodes s left join courier_vendors c on s.courier_id=c.id where s.pincode="' + deliveryPincode + '" and s.lm_status="1" and c.code in (' + courier_code_list + ')';
                                                                                                            dbController.getMysqlDBMaster().then(async function (connection2) {
                                                                                                                connection2.query(sql23, (error, results3) => {
                                                                                                                    if (error) {
                                                                                                                        return res.status(400).json({
                                                                                                                            message: 'Bad Request',
                                                                                                                            error: error.message
                                                                                                                        })
                                                                                                                    } else {
      
      
      
                                                                                                                    }
                                                                                                                    console.log("resultsCouriers@1----", results1)
                                                                                                                    console.log("resultsCouriers@2----", results2)
                                                                                                                    console.log("mastersqldata@3--", results3)
                                                                                                                    results1.forEach(function (data1) {
                                                                                                                        // results3.forEach(function(data){
                                                                                                                        const isFound1 = results3.some(data => {
                                                                                                                            console.log("results2.courier_code--", data.courier_id)
                                                                                                                            console.log("results3.courier_code--", data1.courier_id)
      
                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                return true;
                                                                                                                            }
                                                                                                                        });
                                                                                                                        console.log("isFound2---", isFound1)
                                                                                                                        if (isFound1 === false) {
      
                                                                                                                            // results1.forEach(function(element){
                                                                                                                            console.log("myid---", data1.courier_id)
                                                                                                                            results3.push({
                                                                                                                                courier_id: data1.courier_id
                                                                                                                            })
                                                                                                                            // })
                                                                                                                        }
      
                                                                                                                        // })
                                                                                                                    })
      
                                                                                                                    results2.forEach(function (data1) {
                                                                                                                        // results3.forEach(function(data){
                                                                                                                        const isFound = results3.some(data => {
                                                                                                                            // console.log("mysql.courier_code--",element.courier_code)
                                                                                                                            if (data.courier_id === data1.courier_id) {
                                                                                                                                return true;
                                                                                                                            }
                                                                                                                        });
                                                                                                                        console.log("isFound---", isFound)
                                                                                                                        if (isFound === true) {
                                                                                                                            let el = results3.find(itm => itm.courier_id === data1.courier_id);
                                                                                                                            if (el)
                                                                                                                                results3.splice(results3.indexOf(el), 1);
                                                                                                                        }
      
                                                                                                                        // })
                                                                                                                    })
                                                                                                                    console.log("results3---", results3)
      
                                                                                                                    commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                                                                        res.json({
                                                                                                                            status: 200,
                                                                                                                            data: newdata2
                                                                                                                        })
                                                                                                                    })
      
      
                                                                                                                })
                                                                                                            })
                                                                                                        }
                                                                                                    })
      
                                                                                                    // }
      
                                                                                                }
      
                                                                                            })
      
      
                                                                                        })
                                                                                        // }else{
                                                                                        //     res.json({
                                                                                        //         status: 400,
                                                                                        //         message: 'Validation: database is required...'
                                                                                        //       })
                                                                                        // }
      
      
      
                                                                                        // res.json({
                                                                                        //   status: 200,
                                                                                        //   data: priorityRules
                                                                                        // })
                                                                                    })
      
      
      
                                                                                }
                                                                            })
                                                                        })
                                                                    }else if(platform.toUpperCase() == 'V2'){
                                                                      console.log("reached on V2")
                                                                      var results3 = []
                                                                      websites2Model.find({
                                                                        _id: tenant_id
                                                                      }).then(function(websites){
                                                                        // console.log("websites---",websites)
                                                                        if(websites!=''){
                                                                          websites.forEach(function(data){
                                                                              data.enabled_couriers.forEach(function(element){
                                                                                results3.push({
                                                                                  courier_id: element
                                                                                })
                                                                              })
                                                                          })
                                                                          console.log("results3--",results3)
                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                              res.json({
                                                                                  status: 200,
                                                                                  data: newdata2
                                                                              })
                                                                          })
                                                                        }else{
                                                                          results3 = []
                                                                          console.log("results3--",results3)
                                                                          commanController.recommendedPriorityRule2(priorityRules, results3).then(function (newdata2) {
                                                                              res.json({
                                                                                  status: 200,
                                                                                  data: newdata2
                                                                              })
                                                                          })
                                                                        }
                                                                      })
                                                                     
                    
                                                                  }
  
                                                                    
  
  
                                                                } else {
                                                                    res.json({
                                                                        status: 400,
                                                                        data: 'All fields are required....'
                                                                    })
                                                                }
  
                                                            })
                                                        }
                                                    }
                                                })
  
                                            } else {
                                                commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                                                    // console.log("priorityRules---", priorityRules)
                                                    res.json({
                                                        status: 400,
                                                        data: priorityRules
                                                    })
                                                })
                                            }
                                        }
  
                                    }
  
                                })
                            }
                        })
                    } else {
                        commanController.recommendedPriorityRule(req, zone).then(function (priorityRules) {
                            // console.log("priorityRules---", priorityRules)
                            res.json({
                                status: 400,
                                data: priorityRules
                            })
                        })
                    }
                }
  
            })
  
        }).catch(function (err) {
            return res.status(500).json({
                status: false,
                error: err.message
            })
        })
    } else {
        return res.status(400).json({
            message: 'All field are required..'
        })
    }
  
  }

/*
    Search client name of v1 and v2 platform api [v1 use system master (mysql)database and hostnames collection fqdn column]
    [v2 shiplink_staging (mongodb)database websites table]
*/
exports.searchClientName = async (req, res, next) => {
  if (req.body.client_name && (req.body.client_name !== undefined || req.body.client_name !== 'undefined' || req.body.client_name !== '')) {
    var client_name = trim(req.body.client_name)
    var allClient = [];
    try {
      const searchData = await websites2Model.find({ 'mainvendor': { '$regex': client_name } })  // V2 client_name check
      var sql = "select 'V1' as platform,h.fqdn as client_name,h.website_id as tenant_id,w.uuid as database_name from hostnames h left join websites w on h.website_id=w.id where h.fqdn like '" + client_name + "%' and h.fqdn like '%instashipin%' and h.under_maintenance_since is null";
      dbController.getMysqlDBMaster().then(async function (connection2) {   // V1 client_name check
        connection2.query(sql, (error, results) => {
          if (error) {
            return res.status(400).json({
              message: 'Bad Request',
            })
          } else if (results && (results != undefined || results != 'undefined' || results != '')) {
            results.forEach(function(data){
              allClient.push({                            // V1 clients data push on array
                platform: data.platform,
                client_name: data.client_name,
                tenant_id: data.tenant_id.toString(),
                database_name: data.database_name
              })
            })
          } else {
            allClient == []
          }
          if (searchData != '') {
            searchData.forEach(function(data){                // V2 clients data push on array
              allClient.push({
                platform: 'V2',
                client_name: data.mainvendor,
                tenant_id: data._id,
                database_name: 'instashipin'
              })
            })
            
          } else {
            allClient == []
          }
          if (allClient != '') {
            res.json({
              status: 200,
              message: 'Success',
              data: allClient
            })
          } else {
            res.json({
              status: 400,
              message: 'Not matching data',
              data: []
            })
          }
        })
      })

    } catch (error) {
      return res.status(500).json({
        status: false,
        error: error.message
      })
    }
  } else {
    res.json({
      status: 400,
      message: 'Please enter client name....'
    })
  }
}

/*
    Validation api for checking the added data already exists or not.
*/

exports.validationPriorityRules = async (req, res, next) => {
  if (req.body.tenant_id == undefined) {
    res.json({
      message: 'Required all fields....'
    })
  }else{
    if (req.body.pay_type != undefined) {
      if (req.body.rule_code == 'WZ') {
        if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
          res.json({
            message: 'Validation: Required zone, weight and priority...'
          })
        }else{
          var tenant_id = trim(req.body.tenant_id)
          var pay_type = trim(req.body.pay_type)
          var rule_code = trim(req.body.rule_code)
          var zone = trim(req.body.zone)
          var weight = trim(req.body.weight)
          var arr = weight.split("-");         
          var from =  trim(arr[0])
          var to = trim(arr[1])
          try {
            const results = await priorityRulesModel.find({ 
                tenant_id: tenant_id,            //check this related data in collection
                rule_code: rule_code,
                weight: weight,
                pay_type: pay_type,
                zone: zone,
                from: from,
                to: to
             })
             if(results != ''){
              res.json({
                error: true,
                message: 'Data Already Exist'
              });
             }else{
              res.json({
                error: false,
              });
             }
           } catch (error) {
            // console.log(`Error: ${error.message}`)
            return res.status(500).json({
              error: err.message
            })
          }
        }
      }else if (req.body.rule_code == 'ZM') {
        if (req.body.zone == undefined || req.body.list == undefined) {
          res.json({
            message: 'Validation: Required zone and priority...'
          })
        }else {
          var tenant_id = trim(req.body.tenant_id)
          var pay_type = trim(req.body.pay_type)
          var rule_code = trim(req.body.rule_code)
          var zone = trim(req.body.zone)
          try {
            const results = await priorityRulesModel.find({ 
              tenant_id: tenant_id,            //check this related data in collection
              rule_code: rule_code,
              pay_type: pay_type,
              zone: zone,
           })
           if(results != ''){
            res.json({
              error: true,
              message: 'Data Already Exist'
            });
           }else{
            res.json({
              error: false,
            });
           }
          }catch (error) {
            // console.log(`Error: ${error.message}`)
            return res.status(500).json({
              error: err.message
            })
          }
        }
      }else if (req.body.rule_code == 'WM') {
        if (req.body.weight == undefined || req.body.list == undefined) {
          res.json({
            message: 'Validation: Required weight and priority...'
          })
        }else{
          var tenant_id = trim(req.body.tenant_id)
          var pay_type = trim(req.body.pay_type)
          var rule_code = trim(req.body.rule_code)
          var weight = trim(req.body.weight)
          var arr = weight.split("-");         
          var from =  trim(arr[0])
          var to = trim(arr[1])
          try{
            const results = await priorityRulesModel.find({ 
              tenant_id: tenant_id,            //check this related data in collection
              rule_code: rule_code,
              weight: weight,
              pay_type: pay_type,
              from: from,
              to:to
           })
           if(results != ''){
            res.json({
              error: true,
              message: 'Data Already Exist'
            });
           }else{
            res.json({
              error: false,
            });
           }
          }catch (error) {
            // console.log(`Error: ${error.message}`)
            return res.status(500).json({
              error: err.message
            })
          }
        }
      }else if (req.body.list) {
        var tenant_id = trim(req.body.tenant_id)
        var pay_type = trim(req.body.pay_type)
        try{
          const results = await priorityRulesModel.find({ 
            tenant_id: tenant_id,
            pay_type: pay_type,
            rule_code:'default_rule'
         })
         if(results != ''){
          res.json({
            error: true,
            message: 'Data Already Exist'
          });
         }else{
          res.json({
            error: false,
          });
         }
        } catch (error) {
          // console.log(`Error: ${error.message}`)
          return res.status(500).json({
            error: err.message
          })
        }
      }else {
        res.json({
          message: 'Required all fields....'
        })
      }
    }else if (req.body.pay_type == undefined) {
      if (req.body.rule_code == 'WZ') {
        if (req.body.weight == undefined || req.body.zone == undefined || req.body.list == undefined) {
          res.json({
            message: 'Validation: Required zone, weight and priority...'
          })
        } else {
          var tenant_id = trim(req.body.tenant_id)
          var rule_code = trim(req.body.rule_code)
          var zone = trim(req.body.zone)
          var weight = trim(req.body.weight)
          var arr = weight.split("-");         
          var from =  trim(arr[0])
          var to = trim(arr[1])
          try {
            const results = await priorityRulesModel.find({ 
              tenant_id: tenant_id,            //check this related data in collection
              rule_code: rule_code,
              weight: weight,
              pay_type: 'BOTH',
              zone: zone,
              from:from,
              to:to
             })
             if(results != ''){
              res.json({
                error: true,
                message: 'Data Already Exist'
              });
             }else{
              res.json({
                error: false
              });
             }
            } catch (error) {
              // console.log(`Error: ${error.message}`)
              return res.status(500).json({
                error: err.message
              })
            }
        }
      }else if (req.body.rule_code == 'ZM') {
        if (req.body.zone == undefined || req.body.list == undefined) {
          res.json({
            message: 'Validation: Required zone and priority...'
          })
        }else{
          var tenant_id = trim(req.body.tenant_id)
          var rule_code = trim(req.body.rule_code)
          var zone = trim(req.body.zone)
          try{
            const results = await priorityRulesModel.find({ 
              tenant_id: tenant_id,            //check this related data in collection
              rule_code: rule_code,
              pay_type: 'BOTH',
              zone: zone,
           })
           if(results != ''){
            res.json({
              error: true,
              message: 'Data Already Exist'
            });
           }else{
            res.json({
              error: false,
            });
           }
          }catch (error) {
            // console.log(`Error: ${error.message}`)
            return res.status(500).json({
              error: err.message
            })
          }
        }
      }else if (req.body.rule_code == 'WM') {
        if (req.body.weight == undefined || req.body.list == undefined) {
          res.json({
            message: 'Validation: Required weight and priority...'
          })
        } else {
          var tenant_id = trim(req.body.tenant_id)
          var rule_code = trim(req.body.rule_code)
          var weight = trim(req.body.weight)
          var arr = weight.split("-");         
          var from =  trim(arr[0])
          var to = trim(arr[1])
          try{
            const results = await priorityRulesModel.find({ 
                tenant_id: tenant_id,            //check this related data in collection
                rule_code: rule_code,
                weight: weight,
                pay_type: 'BOTH',
                from: from,
                to: to
           })
           if(results != ''){
            res.json({
              error: true,
              message: 'Data Already Exist'
            });
           }else{
            res.json({
              error: false,
            });
           }
          }catch (error) {
            // console.log(`Error: ${error.message}`)
            return res.status(500).json({
              error: err.message
            })
          }
        }
      }else if (req.body.list) {
        var tenant_id = trim(req.body.tenant_id)
        try{
          const results = await priorityRulesModel.find({
            tenant_id: tenant_id,
            pay_type: 'BOTH',
            rule_code:'default_rule'
         })
         if(results != ''){
          res.json({
            error: true,
            message: 'Data Already Exist'
          });
         }else{
          res.json({
            error: false,
          });
         }
        } catch (error) {
          // console.log(`Error: ${error.message}`)
          return res.status(500).json({
            error: err.message
          })
        }
      }else {
        res.json({
          message: 'Required all fields....'
        })
      }
    }
  }
}

/*
    This api used for deleted priority rules by using its tenant_id and rule_code.
*/

exports.removePriorityRules = async (req, res) => {
try{
  if(req.body.tenant_id && (req.body.tenant_id !== undefined || req.body.tenant_id !== 'undefined' || req.body.tenant_id !== '') &&
  req.body._id && (req.body._id !== undefined || req.body._id !== 'undefined' || req.body._id !== '')){
  var _id = trim(req.body._id)
  var tenant_id = trim(req.body.tenant_id)
  var removeData = []
  var query = {
    _id: _id,
    tenant_id: tenant_id
  }
   removeData = await priorityRulesModel.find({
    _id: _id,
    tenant_id: tenant_id
  })
  if(removeData.length != 0){
    priorityRulesModel.findOneAndRemove(query).exec(function(err,priorityData){
      if(err){
        return res.status(500).json({
          message:'Internal server error!!!....'
        });
      }else{
        res.json({
          status:200,
          data:priorityData,
          message:'Deleted Successfully!!!....'
        });
      }

    });
  } else if(removeData.length == 0){
    res.json({
      status:400,
      message:'Bad Request!!!....'
    });
  }
  }else{
    res.json({
      message:'Select the rule you want to deleted!!!....'
    });
  }
  
}catch (error) {
  // console.log(`Error: ${error.message}`)
  return res.status(500).json({
    error: err.message
  })
}
 

}

/*
    protect middleware to all apis  (Date: 26-02-2022)
*/
exports.protect = async (req, res, next) => {
  try {
    // 1) check if the token is there
    let token
    // console.log("reached hear---",req.headers)
    if (req.headers.authorization && req.headers.authorization.startsWith("Bearer")) {
      token = req.headers.authorization.split(" ")[1];
      // console.log("token---",token)
    }
    if (!token) {
      res.json({
        status: 401,
        message: 'No token provided'
      })
    } else {

      // 2) Verify token
      const decode = await promisify(jwt.verify)(token, process.env.JWT_SECRET,);
      // console.log("decode---",decode)

      // 3) check if the user is exist (not deleted)

      const user = await websitesModel.find({ "tenant_id": decode.tenant_id });
      // console.log("user---",user)
      if (user == '') {
        res.json({
          status: 401,
          message: 'Invalid token'
        })
      } else {
        req.user = user;
        next();
      }
    }
  } catch (error) {
    // console.log(`Error: ${error.message}`)
    return res.status(401).json({
      message: 'Token Expired',
    })
  }
};