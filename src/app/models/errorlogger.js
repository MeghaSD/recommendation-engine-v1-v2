var mongoose = require('mongoose');
const dotenv = require('dotenv');
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI;
var conn = mongoose.createConnection(database);
var ErrorLoggerSchema = new mongoose.Schema({
    time:{type:String,default:"",required:false},
    url:{type:String,default:"",required:false},
    status:{type:String,default:"",required:false},
    message:{type:String,default:"",required:false},
})
module.exports = conn.model('ErrorLogger', ErrorLoggerSchema);
// mongoose.model('ErrorLogger',ErrorLoggerSchema);